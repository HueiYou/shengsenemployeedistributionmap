﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.UniForm
{
    public class OutUniResult
    {
        public OutUniResult(HttpStatusCode code, object data, object err)
        {
            StatusCode = code;
            Result = data;
            Error = err;
        }
        public HttpStatusCode StatusCode { get; set; } //統一回傳的狀態碼
        public object Result { get; set; } //回傳的物件
        public object Error { get; set; } //統一回傳的錯誤訊息摘要
    }
}