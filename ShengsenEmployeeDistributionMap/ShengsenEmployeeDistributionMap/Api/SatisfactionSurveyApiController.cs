﻿using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using ShengsenEmployeeDistributionMap.Models.ViewModel.UniForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;

namespace ShengsenEmployeeDistributionMap.Api
{
    public class SatisfactionSurveyApiController : ApiController
    {
        private readonly SatisfactionSurveyBLO _SatisfactionSurveyBLO;
        /// <summary>
        /// Constructor
        /// </summary>
        public SatisfactionSurveyApiController()
        {
            _SatisfactionSurveyBLO = new SatisfactionSurveyBLO();
        }

        [HttpGet]
        public OutUniResult GetSatisfactionSurveyData([FromUri] string Id)
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _SatisfactionSurveyBLO.GetSatisfactionSurvey(Id), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }

        [HttpPost]
        public OutUniResult UpdateSatisfactionSurvey([FromBody]InSatisfactionSurveyPageViewModel model)
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _SatisfactionSurveyBLO.UpdateSatisfactionSurvey(model), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public OutUniResult SendSatisfactionSurveyInvitation([FromBody]InSatisfactionSurveyViewModel model)
        {
            try
            {
                model.CurrentUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "");
                return new OutUniResult(HttpStatusCode.OK, _SatisfactionSurveyBLO.SendIntitation(model), null);
            }
            catch(Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }
    }
}