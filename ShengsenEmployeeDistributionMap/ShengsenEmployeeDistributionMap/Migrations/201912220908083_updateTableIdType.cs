namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateTableIdType : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Employees");
            DropPrimaryKey("dbo.LivingArea");
            DropPrimaryKey("dbo.StationedCompany");
            AlterColumn("dbo.Employees", "Id", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.LivingArea", "Id", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.StationedCompany", "Id", c => c.String(nullable: false, maxLength: 50));
            AddPrimaryKey("dbo.Employees", "Id");
            AddPrimaryKey("dbo.LivingArea", "Id");
            AddPrimaryKey("dbo.StationedCompany", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.StationedCompany");
            DropPrimaryKey("dbo.LivingArea");
            DropPrimaryKey("dbo.Employees");
            AlterColumn("dbo.StationedCompany", "Id", c => c.String(nullable: false, maxLength: 16));
            AlterColumn("dbo.LivingArea", "Id", c => c.String(nullable: false, maxLength: 16));
            AlterColumn("dbo.Employees", "Id", c => c.String(nullable: false, maxLength: 16));
            AddPrimaryKey("dbo.StationedCompany", "Id");
            AddPrimaryKey("dbo.LivingArea", "Id");
            AddPrimaryKey("dbo.Employees", "Id");
        }
    }
}
