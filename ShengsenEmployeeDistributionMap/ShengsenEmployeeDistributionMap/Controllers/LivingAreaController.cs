﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShengsenEmployeeDistributionMap.Models;
using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.DMO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;

namespace ShengsenEmployeeDistributionMap.Controllers
{
    /// <summary>
    /// 居住地區
    /// </summary>
    [Authorize]
    public class LivingAreaController : Controller
    {
        private LivingAreaBLO _LivingAreaBLO;
        public LivingAreaController()
        {
            _LivingAreaBLO = new LivingAreaBLO();
        }

        // GET: LivingArea
        public ActionResult Index()
        {
            return View();
        }
    }
}
