﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ShengsenEmployeeDistributionMap.Models;
using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.UniForm;

namespace ShengsenEmployeeDistributionMap.Api
{
    /// <summary>
    /// 居住地區Api
    /// </summary>
    [Authorize]
    public class LivingAreaApiController : ApiController
    {
        private LivingAreaBLO _LivingAreaBLO;
        public LivingAreaApiController()
        {
            _LivingAreaBLO = new LivingAreaBLO();
        }

        /// <summary>
        /// 取得居住地區過濾資料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public OutUniResult GetFilterData(string CityName = "", string DistrictName = "", string PostalCode = "")
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _LivingAreaBLO.GetFilterData(CityName, DistrictName, PostalCode), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }

        /// <summary>
        /// 取得縣市別
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public OutUniResult GetCitySelectListItems()
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _LivingAreaBLO.GetCitySelectListItems(), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }

        /// <summary>
        /// 取得鄉鎮市區別
        /// </summary>
        /// <param name="CityCode">縣市代碼</param>
        /// <returns></returns>
        [AllowAnonymous]
        public OutUniResult GetDistrictSelectListItems(string CityCode)
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _LivingAreaBLO.GetDistrictSelectListItems(CityCode), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }
    }
}