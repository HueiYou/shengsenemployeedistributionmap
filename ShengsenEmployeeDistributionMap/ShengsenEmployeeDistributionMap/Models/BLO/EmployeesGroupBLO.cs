﻿using ShengsenEmployeeDistributionMap.Models.DAO;
using System.Collections.Generic;
using System.Linq;

namespace ShengsenEmployeeDistributionMap.Models.BLO
{
    public class EmployeesGroupBLO
    {
        private readonly EmployeesGroupDAO _empsGroupDao;

        public EmployeesGroupBLO()
        {
            _empsGroupDao = new EmployeesGroupDAO();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<EmployeesGroup> GetAll()
        {
            return _empsGroupDao.GetAll();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(IEnumerable<EmployeesGroup> item)
        {
            return _empsGroupDao.Create(item);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(EmployeesGroup item)
        {
            return _empsGroupDao.Update(item);
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(IEnumerable<EmployeesGroup> item)
        {
            return _empsGroupDao.Delete(item);
        }
    }
}