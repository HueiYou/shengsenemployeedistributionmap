﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.DAO
{
    /// <summary>
    /// 駐點公司DAO
    /// </summary>
    public class StationedCompanyDAO : IDisposable
    {
        private ApplicationDbContext db;
        public StationedCompanyDAO()
        {
            db = new ApplicationDbContext();
        }
        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IQueryable<StationedCompany> GetAll()
        {
            return db.StationedCompany.AsNoTracking().AsQueryable();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(StationedCompany item)
        {
            db.StationedCompany.Add(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(StationedCompany item)
        {
            StationedCompany updateItem = db.StationedCompany.Find(item.Id);
            db.Entry(updateItem).CurrentValues.SetValues(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(StationedCompany item)
        {
            StationedCompany deleteItem = db.StationedCompany.Find(item.Id);
            db.StationedCompany.Remove(deleteItem);
            int result = db.SaveChanges();

            return result;
        }

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }
    }
}