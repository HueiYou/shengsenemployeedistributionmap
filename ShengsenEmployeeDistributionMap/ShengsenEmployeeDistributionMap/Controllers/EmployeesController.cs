﻿using ShengsenEmployeeDistributionMap.Models;
using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.DMO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShengsenEmployeeDistributionMap.Controllers
{
    /// <summary>
    /// 員工
    /// </summary>
    [Authorize]
    public class EmployeesController : Controller
    {
        private EmployeesBLO _EmployeesBLO = new EmployeesBLO();

        public EmployeesController()
        {
            _EmployeesBLO = new EmployeesBLO();
        }

        //Amy: 頁面搜尋條件設定
        // GET: Employees
        public ActionResult Index()
        {
            return View();
        }

        #region 員工新增/編輯

        /// <summary>
        /// 新增/編輯
        /// </summary>
        /// <param name="id">員工編號</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateOrEdit(string id)
        {
            try
            {
                ViewBag.IsExist = !string.IsNullOrEmpty(id);    //是否已存在DB，判斷是新增還是編輯

                //新增
                if (string.IsNullOrEmpty(id))
                {
                    InEmployeesValidationViewModel viewModel = new InEmployeesValidationViewModel();
                    viewModel.Id = id;
                    //開始工作日期設為今日
                    viewModel.StartWorkDateString = DateTime.Now.ToShortDateString();
                    return View(viewModel);
                }
                //編輯
                else
                {
                    Employees employees = _EmployeesBLO.GetAll().FirstOrDefault(x => x.Id.Equals(id));

                    //防呆檢查
                    if (employees == null)
                        return HttpNotFound();
                    else
                        return View(employees.ToInEmployeesValidationViewModel());
                }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Employees/CreateOrEdit
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOrEdit([Bind(Exclude = "CitySelectListItems,DistrictSelectListItems,StationedCompanySelectListItems")] InEmployeesValidationViewModel item)
        {
            try
            {
                #region 設定預設值

                item.CreateDate = DateTime.Now;
                item.BirthdayString = item.Birthday?.ToShortDateString();
                item.StartWorkDateString = item.StartWorkDate.ToShortDateString();
                item.OnBoardDateString = item.OnBoardDate?.ToShortDateString();
                item.LocationedDateString = item.LocationedDate?.ToShortDateString();
                ViewBag.IsExist = !string.IsNullOrEmpty(item.Id);    //是否已存在DB，判斷是新增還是編輯

                #endregion 設定預設值

                //欄位驗證
                if (ModelState.IsValid)
                {
                    int result = 0; //是否成功

                    //新增
                    if (string.IsNullOrEmpty(item.Id))
                    {
                        //設定GUID
                        item.Id = Guid.NewGuid().ToString();

                        result = _EmployeesBLO.Create(item.ToEmployees());
                    }
                    //編輯
                    else
                    {
                        result = _EmployeesBLO.Update(item.ToEmployees());
                    }

                    if (result > 0)
                        return RedirectToAction("Index");
                    else
                        return View(item);
                }

                //驗證失敗的訊息
                var errors = ModelState.Values.Where(x => x.Errors.Count() > 0);

                return View(item);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        #endregion 員工新增/編輯

        #region 匯入功能

        /// <summary>
        /// 匯入員工資料
        /// </summary>
        /// <param name="importFile"></param>
        /// <returns></returns>
        public ActionResult ImportEmployeesData(HttpPostedFileBase importFile)
        {
            try
            {
                //檢查是否上傳檔案
                if (importFile != null)
                {
                    #region 檢查檔案格式

                    //取得副檔名，並檢查是否是EXCEL格式
                    string FileExtension = System.IO.Path.GetExtension(importFile.FileName);
                    if (FileExtension != ".xlsx")
                        throw new Exception("檔案格式不符");

                    //檢查檔案大小是否 > 5 MB
                    if (importFile.ContentLength > 5 * 1024 * 1024)
                        throw new Exception("檔案大小超過5MB");

                    #endregion 檢查檔案格式

                    #region 暫存檔案到excelTemp資料夾

                    string appPath = Path.Combine(Request.PhysicalApplicationPath, "Content\\ExcelTemps");
                    if (!Directory.Exists(appPath))
                    {
                        Directory.CreateDirectory(appPath);
                    }

                    var physicalPath = Path.Combine(appPath, importFile.FileName);
                    importFile.SaveAs(physicalPath);

                    #endregion 暫存檔案到excelTemp資料夾

                    //開始匯入
                    bool isSuccess = _EmployeesBLO.ImportEmployeesData(physicalPath);

                    #region 刪除檔案

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    #endregion 刪除檔案

                    if (isSuccess)
                        return Json("匯入成功", JsonRequestBehavior.AllowGet);
                    else
                        return Json("匯入失敗", JsonRequestBehavior.AllowGet);
                }
                else
                    throw new Exception("請匯入檔案");
            }
            catch (Exception ex)
            {
                if (importFile != null)
                {
                    string appPath = Path.Combine(Request.PhysicalApplicationPath, "Content\\ExcelTemps");
                    if (!Directory.Exists(appPath))
                    {
                        Directory.CreateDirectory(appPath);
                    }

                    var physicalPath = Path.Combine(appPath, importFile.FileName);
                    System.IO.File.Delete(physicalPath);    //刪除檔案
                }
                return Json(ex.Message + (Path.Combine(System.IO.Directory.GetCurrentDirectory(), "Content\\ExcelTemps", importFile.FileName)), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 匯入範本下載
        /// </summary>
        /// <returns></returns>
        public ActionResult ImportTemplateData()
        {
            try
            {
                string filePath = Server.MapPath("~/Content/ExcelTemplate/ImportTemplate.xlsx");
                // 暫存
                string GUID = Guid.NewGuid().ToString();
                TempData[GUID] = _EmployeesBLO.ExportImportTemplate(filePath).ToArray();

                return Json(new { FileGuid = GUID, FileName = "匯入檔案範本.xlsx" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion 匯入功能

        #region 匯出

        /// <summary>
        /// 匯出報表暫存
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportTemplateData()
        {
            try
            {
                string filePath = Server.MapPath("~/Content/ExcelTemplate/ImportTemplate.xlsx");
                // 暫存
                string GUID = Guid.NewGuid().ToString();
                TempData[GUID] = _EmployeesBLO.ExportEmployeeData(filePath).ToArray();

                return Json(new { Status = HttpStatusCode.OK, FileGuid = GUID, FileName = $"員工資訊_{DateTime.Now.ToString("yyyyMMdd")}.xlsx" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = HttpStatusCode.BadRequest, ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 下載
        /// </summary>
        /// <param name="fileGuid">暫存Key</param>
        /// <param name="fileName">下載檔名</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Download(string fileGuid, string fileName)
        {
            try
            {
                if (TempData[fileGuid] != null)
                {
                    byte[] data = TempData[fileGuid] as byte[];
                    return File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
                else
                {
                    return new EmptyResult();
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion 匯出

        #region 求職者新增

        /// <summary>
        /// 求職者新增
        /// </summary>
        /// <param name="id">員工Id</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult JobSeekerViewOrEdit(string id)
        {
            try
            {
                //防呆檢查：一定要有員工Id
                if (string.IsNullOrEmpty(id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //檢查DB是否存在該筆資料
                Employees employees = _EmployeesBLO.GetAll().FirstOrDefault(x => x.Id.Equals(id));

                //新增
                if (employees == null)
                {
                    ViewBag.IsExist = false;    //是否已存在DB
                    InEmployeesValidationViewModel viewModel = new InEmployeesValidationViewModel();
                    viewModel.Id = id;
                    //工程師總年資設為今日
                    viewModel.StartWorkDateString = DateTime.Now.ToShortDateString();
                    return View(viewModel);
                }
                //檢視
                else
                {
                    ViewBag.IsExist = true;    //是否已存在DB
                    return View(employees.ToInEmployeesValidationViewModel());
                }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: Employees/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult JobSeekerViewOrEdit([Bind(Exclude = "CitySelectListItems,DistrictSelectListItems,StationedCompanySelectListItems,Category")] InEmployeesValidationViewModel item)
        {
            try
            {
                #region 設定預設值

                item.CreateDate = DateTime.Now;
                item.Category = 1;  //104
                item.BirthdayString = item.Birthday?.ToShortDateString();
                item.StartWorkDateString = item.StartWorkDate.ToShortDateString();
                item.OnBoardDateString = item.OnBoardDate?.ToShortDateString();
                item.LocationedDateString = item.LocationedDate?.ToShortDateString();
                ViewBag.IsExist = false;    //是否已存在DB

                #endregion 設定預設值

                //欄位驗證
                if (ModelState.IsValid)
                {
                    int result = _EmployeesBLO.Create(item.ToEmployees());
                    if (result > 0)
                        return RedirectToAction("JobSeekerViewOrEditConfirmation");
                    else
                        return View(item);
                }

                //驗證失敗的訊息
                //var errors = ModelState.Values.Where(x => x.Errors.Count() > 0);

                return View(item);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// 求職者新增完顯示的視窗
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult JobSeekerViewOrEditConfirmation()
        {
            return View();
        }

        #endregion 求職者新增
    }
}