﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.DAO
{
    /// <summary>
    /// 員工DAO
    /// </summary>
    public class EmployeesDAO : IDisposable
    {
        private readonly ApplicationDbContext db;
        public EmployeesDAO()
        {
            db = new ApplicationDbContext();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IQueryable<Employees> GetAll()
        {
            return db.Employees.AsNoTracking().AsQueryable();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(Employees item)
        {
            db.Employees.Add(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(Employees item)
        {
            Employees updateItem = db.Employees.Find(item.Id);
            db.Entry(updateItem).CurrentValues.SetValues(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(Employees item)
        {
            Employees deleteItem = db.Employees.Find(item.Id);
            db.Employees.Remove(deleteItem);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 批次新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Creates(IEnumerable<Employees> items)
        {
            db.Employees.AddRange(items);
            int result = db.SaveChanges();

            return result;
        }

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }
    }
}