namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Employees", "StartWorkDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "StartWorkDate", c => c.DateTime(nullable: false));
        }
    }
}
