﻿using System.ComponentModel.DataAnnotations;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.Out
{
    /// <summary>
    /// 員工ViewModel
    /// </summary>
    public class OutEmployeesViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        [Display(Name = "員工姓名")]
        public string EmployeeName { get; set; }

        [Display(Name = "縣市")]
        public string CityCode { get; set; }

        [Display(Name = "鄉、鎮市區")]
        public string DistrictCode { get; set; }

        [Display(Name = "縣市")]
        public string CityName { get; set; }

        [Display(Name = "鄉、鎮市區")]
        public string DistrictName { get; set; }

        [Display(Name = "駐點公司編號")]
        public string CompanyId { get; set; }

        [Display(Name = "駐點公司")]
        public string CompanySimpleName { get; set; }

        [Display(Name = "技能")]
        public string Department { get; set; }

        [Display(Name = "類別(來源)")]
        public int Category { get; set; }

        public string CategoryName { get; set; }

        [Display(Name = "四型人格")]
        public string Personality { get; set; }

        [Display(Name = "學歷/學位")]
        public string Education { get; set; }

        [Display(Name = "經歷")]
        public string WorkExperience { get; set; }

        [Display(Name = "總年資(月份)")]
        public int TotalWorkMonths { get; set; }

        [Display(Name = "住所地址")]
        public string Residence { get; set; }
    }
}