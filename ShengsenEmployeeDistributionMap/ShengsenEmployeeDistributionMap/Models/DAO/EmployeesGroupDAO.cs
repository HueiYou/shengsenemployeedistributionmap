﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShengsenEmployeeDistributionMap.Models.DAO
{
    public class EmployeesGroupDAO : IDisposable
    {
        private readonly ApplicationDbContext db;

        /// <summary>
        /// Constructor
        /// </summary>
        public EmployeesGroupDAO()
        {
            db = new ApplicationDbContext();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IQueryable<EmployeesGroup> GetAll()
        {
            return db.EmployeesGroup.AsNoTracking().AsQueryable();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(IEnumerable<EmployeesGroup> item)
        {
            db.EmployeesGroup.AddRange(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(EmployeesGroup item)
        {
            EmployeesGroup updateItem = db.EmployeesGroup.Find(item.Id);
            db.Entry(updateItem).CurrentValues.SetValues(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 刪除 (ToDo)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(IEnumerable<EmployeesGroup> item)
        {
            IEnumerable<string> idList = item.Select(y => y.Id);
            List<EmployeesGroup> query = db.EmployeesGroup.Where(x => idList.Contains(x.Id)).ToList();
            db.EmployeesGroup.RemoveRange(query);
            int result = db.SaveChanges();

            return result;
        }

        #region Dispose

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        #endregion Dispose
    }
}