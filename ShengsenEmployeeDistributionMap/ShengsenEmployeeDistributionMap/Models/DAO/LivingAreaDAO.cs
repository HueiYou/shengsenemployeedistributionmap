﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.DAO
{
    /// <summary>
    /// 居住地DAO
    /// </summary>
    public class LivingAreaDAO : IDisposable
    {
        private readonly ApplicationDbContext db;
        public LivingAreaDAO()
        {
            db = new ApplicationDbContext();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IQueryable<LivingArea> GetAll()
        {
            return db.LivingArea.AsNoTracking().AsQueryable();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(LivingArea item)
        {
            db.LivingArea.Add(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(LivingArea item)
        {
            LivingArea updateItem = db.LivingArea.Find(item.Id);
            db.Entry(updateItem).CurrentValues.SetValues(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(LivingArea item)
        {
            LivingArea deleteItem = db.LivingArea.Find(item.Id);
            db.LivingArea.Remove(deleteItem);
            int result = db.SaveChanges();

            return result;
        }

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }
    }
}