﻿using ShengsenEmployeeDistributionMap.Models.DAO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShengsenEmployeeDistributionMap.Models.BLO
{
    /// <summary>
    /// 居住地區BLO
    /// </summary>
    public class LivingAreaBLO
    {
        private readonly LivingAreaDAO _LivingAreaDAO;
        public LivingAreaBLO()
        {
            _LivingAreaDAO = new LivingAreaDAO();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<LivingArea> GetAll()
        {
            return _LivingAreaDAO.GetAll().OrderBy(x => x.CityCode).ThenBy(x => x.DistrictCode);
        }

        /// <summary>
        /// 取得縣市別
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetCitySelectListItems()
        {
            List<SelectListItem> listItems = _LivingAreaDAO.GetAll()
                .Select(x => new SelectListItem()
                {
                    Value = x.CityCode,
                    Text = x.CityName,
                })
                .Distinct()
                .OrderBy(x => x.Value)
                .ToList();

            if (listItems == null)
                return new List<SelectListItem>();
            else
                return listItems;
        }

        /// <summary>
        /// 取得鄉鎮市區別
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetDistrictSelectListItems(string CityCode)
        {
            List<SelectListItem> listItems = _LivingAreaDAO.GetAll()
                .Where(x => x.CityCode.Equals(CityCode))
                .Select(x => new SelectListItem()
                {
                    Value = x.DistrictCode,
                    Text = x.DistrictName,
                })
                .OrderBy(x => x.Value)
                .ToList();

            if (listItems == null)
                return new List<SelectListItem>();
            else
                return listItems;
        }

        /// <summary>
        /// 取得居住地區過濾資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<LivingArea> GetFilterData(string CityName, string DistrictName, string PostalCode)
        {
            IQueryable<LivingArea> query = _LivingAreaDAO.GetAll();

            if (!string.IsNullOrEmpty(CityName))
                query = query.Where(x => x.CityName.Contains(CityName));
            if (!string.IsNullOrEmpty(DistrictName))
                query = query.Where(x => x.DistrictName.Contains(DistrictName));
            if (!string.IsNullOrEmpty(PostalCode))
                query = query.Where(x => x.PostalCode.Contains(PostalCode));

            return query.OrderBy(x => x.CityCode).ThenBy(x => x.DistrictCode);
        }
    }
}