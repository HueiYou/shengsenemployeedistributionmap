namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateTable1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.StatuinedCompany", newName: "StationedCompany");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.StationedCompany", newName: "StatuinedCompany");
        }
    }
}
