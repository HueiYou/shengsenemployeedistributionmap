﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShengsenEmployeeDistributionMap.Models
{
    [Table("Group")]
    public partial class Group
    {
        /// <summary>
        /// 群組編號
        /// </summary>
        [Key]
        [StringLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 群組名稱
        /// </summary>
        [Required]
        [StringLength(50)]
        [Description("群組名稱")]
        public string GroupName { get; set; }
    }
}