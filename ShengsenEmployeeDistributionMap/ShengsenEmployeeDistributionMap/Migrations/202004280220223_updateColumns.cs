namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LivingArea", "PostalCode", c => c.String(nullable: false, maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LivingArea", "PostalCode");
        }
    }
}
