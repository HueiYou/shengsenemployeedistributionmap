﻿using System.Collections.Generic;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    public class InGroupCreateViewModel
    {
        /// <summary>
        /// 群組編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 群組名稱
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string StationedCompanyId { get; set; }

        /// <summary>
        /// 員工編號清單
        /// </summary>
        public List<InEmployeesGroupListViewModel> EmployeeIdList { get; set; }
    }
}