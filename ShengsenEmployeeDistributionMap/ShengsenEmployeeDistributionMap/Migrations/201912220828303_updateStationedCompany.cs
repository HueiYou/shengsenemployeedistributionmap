namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateStationedCompany : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StationedCompany", "CompanyName", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StationedCompany", "CompanyName", c => c.String(maxLength: 50));
        }
    }
}
