﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ShengsenEmployeeDistributionMap.Api
{
    /// <summary>
    /// 員工分佈Api
    /// </summary>
    [Authorize]
    public class EmployeesDistributionApiController : ApiController
    {
        // GET: api/EmployeesDistributionApi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/EmployeesDistributionApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/EmployeesDistributionApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/EmployeesDistributionApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/EmployeesDistributionApi/5
        public void Delete(int id)
        {
        }
    }
}
