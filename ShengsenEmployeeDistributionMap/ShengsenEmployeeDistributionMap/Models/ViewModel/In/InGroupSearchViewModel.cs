﻿namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    public class InGroupSearchViewModel
    {
        /// <summary>
        /// 群組編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 查詢字串
        /// </summary>
        public string QueryString { get; set; }

        /// <summary>
        /// 公司編號
        /// </summary>
        public string CompanyId { get; set; }
    }
}