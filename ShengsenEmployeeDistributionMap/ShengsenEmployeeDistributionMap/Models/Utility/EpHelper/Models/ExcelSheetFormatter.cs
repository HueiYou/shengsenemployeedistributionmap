﻿using System.Collections.Generic;
using OfficeOpenXml.Table;

namespace ShengsenEmployeeDistributionMap.Models.Utility.EpHelper.Model
{
    public class ExcelSheetFormatter<T>
    {
        public ExcelSheetFormatter()
        {
        }

        public ExcelSheetFormatter(string sheetName, List<T> sheetData, string[] tittle)
        {
            SheetName = sheetName;
            SheetData = sheetData;
            Title = tittle;
        }
        /// <summary>
        ///     分頁名稱
        /// </summary>
        public string SheetName { get; set; }

        /// <summary>
        ///     資料集合
        /// </summary>
        public List<T> SheetData { get; set; }

        /// <summary>
        ///     第一欄的標題
        /// 優先權級大於model定義[DisplayName]
        /// 不需要則不用給值
        /// </summary>
        public string[] Title { get; set; }
        /// <summary>
        /// 欄位的格式 支援的格式
        ///        "General";
        ///        "0";
        ///        "0.00";
        ///        "#,##0";
        ///        "#,##0.00";
        ///        "0%";
        ///        "0.00%";
        ///        "0.00E+00";
        ///        "# ?/?";
        ///        "# ??/??";
        ///        "mm-dd-yy";
        ///        "d-mmm-yy";
        ///        "d-mmm";
        ///        "mmm-yy";
        ///        "h:mm AM/PM";
        ///        "h:mm:ss AM/PM";
        ///        "h:mm";
        ///        "h:mm:ss";
        ///        "m/d/yy h:mm";
        ///        "#,##0 ;(#,##0)";
        ///        "#,##0 ;[Red](#,##0)";
        ///        "#,##0.00;(#,##0.00)";
        ///        "#,##0.00;[Red](#,##0.00)";
        ///        "mm:ss";
        ///        "[h]:mm:ss";
        ///        "mmss.0";
        ///        "##0.0";
        ///        "@";
        /// </summary>
        public string[] Format { get; set; }
        /// <summary>
        /// EXCEL 格式化表格之列舉可參考 EXCEL範本編號
        /// </summary>
        public TableStyles TableStyles { get; set; } = TableStyles.None;
        /// <summary>
        /// 鎖定第一列欄位名稱
        /// </summary>
        public bool FreezeFirstRow { get; set; } = true;


    }
}