﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    /// <summary>
    /// 員工資訊搜尋條件ViewModel
    /// </summary>
    public class InEmployeesSearchViewModel
    {
        /// <summary>
        /// 員工姓名
        /// </summary>
        public string EmployeeName { get; set; }

        /// <summary>
        /// 縣市代碼
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 鄉鎮代碼
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 技能
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 駐點公司簡稱
        /// </summary>
        public string CompanySimpleName { get; set; }

        /// <summary>
        /// 類別(來源)
        /// </summary>
        public int? Category { get; set; }

        /// <summary>
        /// 四型人格
        /// </summary>
        public string Personality { get; set; }

        /// <summary>
        /// 學歷/學位
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// 經歷
        /// </summary>
        public string WorkExperience { get; set; }

        /// <summary>
        /// 總年資(月份)_起
        /// </summary>
        public int? TotalWorkMonthsStart { get; set; }

        /// <summary>
        /// 總年資(月份)_迄
        /// </summary>
        public int? TotalWorkMonthsEnd { get; set; }

        /// <summary>
        /// 住所地址
        /// </summary>
        public string Residence { get; set; }
    }
}