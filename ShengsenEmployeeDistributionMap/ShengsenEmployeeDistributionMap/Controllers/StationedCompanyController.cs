﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShengsenEmployeeDistributionMap.Models;
using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.DMO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;

namespace ShengsenEmployeeDistributionMap.Controllers
{
    /// <summary>
    /// 駐點公司
    /// </summary>
    [Authorize]
    public class StationedCompanyController : Controller
    {
        private StationedCompanyBLO _StationedCompanyBLO;
        public StationedCompanyController()
        {
            _StationedCompanyBLO = new StationedCompanyBLO();
        }

        // GET: StationedCompany
        public ActionResult Index()
        {
            return View();
        }

        // GET: StationedCompany/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StationedCompany/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CompanySimpleName,CompanyName,CompanyAddress")] InStationedCompanyValidationViewModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //設定參數值
                    item.Id = Guid.NewGuid().ToString();

                    int result = _StationedCompanyBLO.Create(item.ToStationedCompany());
                    if (result > 0)
                        return RedirectToAction("Index");
                    else
                        return View(item);
                }

                return View(item);
            }
            catch (Exception ex)
            {
                return View(item);
            }
        }

        // GET: StationedCompany/Edit/5
        public ActionResult Edit(string id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                StationedCompany stationedCompany = _StationedCompanyBLO.GetAll().FirstOrDefault(x => x.Id.Equals(id));
                if (stationedCompany == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    return View(stationedCompany.ToInStationedCompanyValidationViewModel());
                }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        // POST: StationedCompany/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CompanySimpleName,CompanyName,CompanyAddress")] InStationedCompanyValidationViewModel item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int result = _StationedCompanyBLO.Update(item.ToStationedCompany());
                    if (result > 0)
                        return RedirectToAction("Index");
                    else
                        return View(item);
                }
                return View(item);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}
