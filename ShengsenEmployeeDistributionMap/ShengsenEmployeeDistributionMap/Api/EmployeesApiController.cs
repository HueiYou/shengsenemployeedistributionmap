﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ShengsenEmployeeDistributionMap.Models;
using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using ShengsenEmployeeDistributionMap.Models.ViewModel.UniForm;

namespace ShengsenEmployeeDistributionMap.Api
{
    /// <summary>
    /// 員工Api
    /// </summary>
    [Authorize]
    public class EmployeesApiController : ApiController
    {
        private EmployeesBLO _EmployeesBLO = new EmployeesBLO();

        public EmployeesApiController()
        {
            _EmployeesBLO = new EmployeesBLO();
        }

        /// <summary>
        /// 取得員工資訊所有資料
        /// </summary>
        /// <param name="searchModel">搜尋條件</param>
        /// <returns></returns>
        [HttpGet]
        public OutUniResult GetEmployeesViewModel([FromUri]InEmployeesSearchViewModel searchModel = null)
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _EmployeesBLO.GetEmployeesViewModel(searchModel), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }

        [HttpPost]
        public OutUniResult GetEmployeesGroupList([FromBody]InEmployeesGroupSearchViewModel searchModel)
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _EmployeesBLO.GetEmployeesGroupList(searchModel), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Delete([FromBody]Employees item)
        {
            try
            {
                if(string.IsNullOrEmpty(item.Id))
                    return new HttpResponseMessage(HttpStatusCode.NotFound);

                int result = _EmployeesBLO.Delete(item);
                if (result > 0)
                    return new HttpResponseMessage(HttpStatusCode.OK);
                else
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("error:" + ex.Message)
                };
            }
        }

        [HttpPost]
        public OutUniResult MailGenerator([FromUri]string MailString)
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _EmployeesBLO.MailGenerator(MailString), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }
    }
}