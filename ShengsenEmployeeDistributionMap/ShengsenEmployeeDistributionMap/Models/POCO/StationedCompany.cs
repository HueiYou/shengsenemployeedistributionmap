﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models
{
    [Table("StationedCompany")]
    public partial class StationedCompany
    {
        [Key]
        [StringLength(50)]
        public string Id { get; set; }

        [Required]
        [StringLength(50)]
        [Description("駐點公司簡稱")]
        public string CompanySimpleName { get; set; }

        [Required]
        [StringLength(50)]
        [Description("駐點公司名稱")]
        public string CompanyName { get; set; }

        [StringLength(150)]
        [Description("駐點公司地址")]
        public string CompanyAddress { get; set; }
    }
}