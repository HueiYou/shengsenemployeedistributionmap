﻿using System.Collections.Generic;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.Out
{
    public class OutGroupViewModel
    {
        /// <summary>
        /// 群組編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 群組名稱
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 員工清單
        /// </summary>
        public List<EmployeeList> Employees { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string StationedCompanyId { get; set; }

        /// <summary>
        /// 駐點公司名稱
        /// </summary>
        public string StationedCompanyName { get; set; }

        /// <summary>
        /// 群組下員工清單
        /// </summary>
        public class EmployeeList
        {
            /// <summary>
            /// 員工編號
            /// </summary>
            public string EmployeeId { get; set; }

            /// <summary>
            /// 員工名稱
            /// </summary>
            public string EmployeeName { get; set; }
        }
    }
}