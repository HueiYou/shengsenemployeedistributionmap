﻿using ReportSystem.Models;
using ShengsenEmployeeDistributionMap.Models.DAO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using ShengsenEmployeeDistributionMap.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace ShengsenEmployeeDistributionMap.Models.BLO
{
    public class SatisfactionSurveyBLO
    {
        private readonly EmployeesBLO _EmployeesBLO;
        private readonly StationedCompanyBLO _StationedCompanyBLO;
        private readonly SatisfactionSurveyDAO _SatisfactionSurveyDAO;

        /// <summary>
        /// Constructor
        /// </summary>
        public SatisfactionSurveyBLO()
        {
            _SatisfactionSurveyDAO = new SatisfactionSurveyDAO();
            _EmployeesBLO = new EmployeesBLO();
            _StationedCompanyBLO = new StationedCompanyBLO();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<SatisfactionSurvey> GetAll()
        {
            return _SatisfactionSurveyDAO.GetAll();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(List<SatisfactionSurvey> item)
        {
            return _SatisfactionSurveyDAO.Create(item);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(SatisfactionSurvey item)
        {
            return _SatisfactionSurveyDAO.Update(item);
        }

        /// <summary>
        /// 取得客戶滿意度調查表(個人)
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public OutSatisfactionSurveyPageViewModel GetSatisfactionSurvey(string Id)
        {
            var SurveyData = GetAll().Where(x => x.Id == Id).ToList();
            var eData = _EmployeesBLO.GetAll().ToList();
            var sData = _StationedCompanyBLO.GetAll().ToList();

            OutSatisfactionSurveyPageViewModel result = SurveyData.Join(eData,
                s => s.EmployeeId,
                e => e.Id,
                (s, e) => new { survey = s, employee = e })
                .Join(sData,
                su => su.survey.StationedCompanyId,
                s => s.Id,
                (su, s) => new { su.survey, su.employee, station = s })
                .Select(x => new OutSatisfactionSurveyPageViewModel
                {
                    Id = x.survey.Id,
                    EmployeeId = x.survey.EmployeeId,
                    EmployeeName = x.employee.EmployeeName,
                    StationedCompanyId = x.survey.StationedCompanyId,
                    StationedName = x.station.CompanyName,
                    EvaluateDate = x.survey.EvaluateDate,
                    Appraiser = x.survey.Appraiser,
                    AppraiseDate = x.survey.AppraiseDate
                }).First();

            return result;
        }

        public HttpStatusCode UpdateSatisfactionSurvey(InSatisfactionSurveyPageViewModel item)
        {
            var SurveyData = GetAll().Where(x => x.Id == item.Id).ToList();
            HttpStatusCode result = HttpStatusCode.BadRequest;

            if (SurveyData.First().Appraiser == null)
            {
                int updateQuantity = Update(new SatisfactionSurvey
                {
                    Id = item.Id,
                    EmployeeId = item.EmployeeId,
                    StationedCompanyId = item.CompanyId,
                    EvaluateDate = item.EvaluateDate,
                    EnthusiasmScore = item.EnthusiasmScore,
                    EnthusiasmDesc = item.EnthusiasmDesc,
                    CommunicationScore = item.CommunicationScore,
                    CommunicationDesc = item.CommunicationDesc,
                    ResolveScore = item.ResolveScore,
                    ResolveDesc = item.ResolveDesc,
                    ProfessionScore = item.ProfessionScore,
                    ProfessionDesc = item.ProfessionDesc,
                    TimeControlScore = item.TimeControlScore,
                    TimeControlDesc = item.TimeControlDesc,
                    Appraiser = item.Appraiser,
                    AppraiseDate = item.AppraiseDate,
                    Comment = item.Comment
                });

                result = updateQuantity > 0 ? HttpStatusCode.OK : HttpStatusCode.NotModified;
            }
            else
            {
                result = HttpStatusCode.PreconditionFailed;
            }

            return result;
        }

        /// <summary>
        /// 寄送滿意度調查表連結
        /// </summary>
        /// <returns></returns>
        public bool SendIntitation(InSatisfactionSurveyViewModel model)
        {
            try
            {
                #region 預建滿意度調查表資料

                // 查詢是否已建立該客戶當期滿意度調查表
                var check = GetAll().Where(x => x.StationedCompanyId == model.StationedCompanyId &&
                                                x.EvaluateDate == model.EvaluateDate).ToList();
                List<SatisfactionSurvey> createList = new List<SatisfactionSurvey>();

                foreach (var empId in model.EmployeeIdList)
                {
                    //if (!check.Where(x => x.EmployeeId == empId).Any(x => x.EmployeeId == empId))
                    if (!check.Any(x => x.EmployeeId == empId))

                    {
                        createList.Add(new SatisfactionSurvey
                        {
                            Id = Guid.NewGuid().ToString(),
                            EmployeeId = empId,
                            StationedCompanyId = model.StationedCompanyId,
                            EvaluateDate = model.EvaluateDate
                        });
                    }
                }
                Create(createList);

                #endregion 預建滿意度調查表資料

                #region 組成信件字串

                List<SatisfactionSurvey> sData = GetAll().Where(x => x.StationedCompanyId == model.StationedCompanyId && x.EvaluateDate == model.EvaluateDate).ToList();
                List<Employees> eList = _EmployeesBLO.GetAll().ToList();
                var SatisfactionData = sData.Join(eList,
                    s => s.EmployeeId,
                    e => e.Id,
                    (s, e) => new { satisfaction = s, employee = e });

                StringBuilder mailContent = new StringBuilder("聖森雲端科技股份有限公司-客戶滿意調查表網址: <br/>");

                foreach (var data in SatisfactionData)
                {
                    mailContent.Append($@"{data.employee.EmployeeName}: <br/>{model.CurrentUrl}/SatisfactionSurvey/SurveyPage?id={data.satisfaction.Id} <br/>");
                }

                #endregion 組成信件字串

                #region 信件寄送

                string[] MailList = model.MailAddress.Split(',');

                foreach (string mail in MailList)
                {
                    MailMessage Mail = new MailMessage
                    {
                        From = new MailAddress("mis@sheng-sen.com", "聖森雲端科技股份有限公司"),
                        Priority = MailPriority.Normal,
                        Subject = "聖森雲端科技股份有限公司-客戶滿意調查表",
                        Body = mailContent.ToString(),
                        IsBodyHtml = true
                    };

                    Mail.To.Add(mail);
                    SMTPHelper.SendMail(Mail);
                }

                #endregion 信件寄送

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}