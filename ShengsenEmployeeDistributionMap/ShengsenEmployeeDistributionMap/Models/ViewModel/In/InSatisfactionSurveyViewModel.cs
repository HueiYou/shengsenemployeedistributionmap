﻿using System;
using System.Collections.Generic;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    public class InSatisfactionSurveyViewModel
    {
        /// <summary>
        /// 員工編號
        /// </summary>
        public List<string> EmployeeIdList { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string StationedCompanyId { get; set; }

        /// <summary>
        /// 評估期間
        /// </summary>
        public DateTime EvaluateDate { get; set; }

        /// <summary>
        /// 寄送信箱
        /// </summary>
        public string MailAddress { get; set; }

        /// <summary>
        /// 當前網域
        /// </summary>
        public string CurrentUrl { get; set; }
    }
}