namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Employees欄位異動 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employees", "WorkExperience", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employees", "WorkExperience", c => c.String(nullable: false));
        }
    }
}
