namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployeesGroup",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 50),
                        GroupId = c.String(nullable: false, maxLength: 50),
                        EmployeeId = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Group", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.GroupId);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 50),
                        GroupName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StationedCompanyGroup",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 50),
                        GroupId = c.String(nullable: false, maxLength: 50),
                        StationedCompanyId = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Group", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.GroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StationedCompanyGroup", "GroupId", "dbo.Group");
            DropForeignKey("dbo.EmployeesGroup", "GroupId", "dbo.Group");
            DropIndex("dbo.StationedCompanyGroup", new[] { "GroupId" });
            DropIndex("dbo.EmployeesGroup", new[] { "GroupId" });
            DropTable("dbo.StationedCompanyGroup");
            DropTable("dbo.Group");
            DropTable("dbo.EmployeesGroup");
        }
    }
}
