﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShengsenEmployeeDistributionMap.Controllers
{
    /// <summary>
    /// 員工分佈
    /// </summary>
    [Authorize]
    public class EmployeesDistributionController : Controller
    {
        // GET: EmployeesDistribution
        public ActionResult Index()
        {
            return View();
        }
    }
}