﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models
{
    /// <summary>
    /// 員工資訊 POCO
    /// </summary>
    public partial class Employees
    {
        [Key]
        [StringLength(50)]
        public string Id { get; set; }

        [Required]
        [StringLength(50)]
        [Description("員工姓名")]
        public string EmployeeName { get; set; }

        [Required]
        [StringLength(10)]
        [Description("縣市代碼")]
        public string CityCode { get; set; }

        [Required]
        [StringLength(10)]
        [Description("鄉、鎮市區代碼")]
        public string DistrictCode { get; set; }

        [StringLength(50)]
        [Description("駐點公司代碼")]
        public string CompanyId { get; set; }

        [Required]
        [StringLength(10)]
        [Description("技能")]
        public string Department { get; set; }

        [Required]
        [Description("類別(來源)")]
        public int Category { get; set; }

        [StringLength(20)]
        [Description("四型人格")]
        public string Personality { get; set; }

        [Required]
        [StringLength(100)]
        [Description("學歷/學位")]
        public string Education { get; set; }
        
        [MaxLength]
        [Description("經歷")]
        public string WorkExperience { get; set; }

        [Required]
        [Description("工程師總年資")]
        [Column(TypeName = "Date")]
        public DateTime StartWorkDate { get; set; }

        [MaxLength]
        [Description("住所地址")]
        public string Residence { get; set; }

        [Required]
        [Description("性別")]
        public int Gender { get; set; }

        [MaxLength]
        [Description("技能描述")]
        public string SkillDescription { get; set; }

        [MaxLength]
        [Description("備註")]
        public string Remark { get; set; }

        [Description("建立日期")]
        [Column(TypeName = "Date")]
        public DateTime? CreateDate { get; set; }

        [Description("聖森到職日期")]
        [Column(TypeName = "Date")]
        public DateTime? OnBoardDate { get; set; }

        [Description("駐點日期")]
        [Column(TypeName = "Date")]
        public DateTime? LocationedDate { get; set; }

        [Description("出生年月日")]
        [Column(TypeName = "Date")]
        public DateTime? Birthday { get; set; }
    }
}