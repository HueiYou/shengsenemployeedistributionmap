﻿using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.DMO
{
    /// <summary>
    /// 居住地區DMO
    /// </summary>
    public static class LivingAreaDMO
    {
        /// <summary>
        /// 將POCO轉成ViewModel
        /// </summary>
        /// <param name="livingArea"></param>
        /// <returns></returns>
        public static InLivingAreaValidationViewModel ToInLivingAreaValidationViewModel(this LivingArea livingArea)
        {
            InLivingAreaValidationViewModel result = new InLivingAreaValidationViewModel()
            {
                Id = livingArea.Id,
                CityCode = livingArea.CityCode,
                CityName = livingArea.CityName,
                DistrictCode = livingArea.DistrictCode,
                DistrictName = livingArea.DistrictName,
                PostalCode = livingArea.PostalCode,
            };

            return result;
        }

        /// <summary>
        /// 將ViewModel轉成POCO
        /// </summary>
        /// <param name="livingArea"></param>
        /// <returns></returns>
        public static LivingArea ToLivingArea(this InLivingAreaValidationViewModel livingArea)
        {
            LivingArea result = new LivingArea()
            {
                Id = livingArea.Id,
                CityCode = livingArea.CityCode,
                CityName = livingArea.CityName,
                DistrictCode = livingArea.DistrictCode,
                DistrictName = livingArea.DistrictName,
                PostalCode = livingArea.PostalCode,
            };

            return result;
        }
    }
}