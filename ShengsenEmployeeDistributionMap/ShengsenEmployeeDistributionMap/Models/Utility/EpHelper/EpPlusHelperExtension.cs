﻿using ShengsenEmployeeDistributionMap.Models.Utility.EpHelper.Model;
using OfficeOpenXml;

namespace ShengsenEmployeeDistributionMap.Models.Utility.EpHelper
{
    public static class EpPlusHelperExtension
    {
        /// <summary>
        /// 寫入表格
        /// </summary>
        /// <typeparam name="T">依照class順序寫入表格</typeparam>
        /// <param name="ep"></param>
        /// <param name="Quantity">工作表，從1開始編碼</param>
        /// <param name="source">資料集合</param>
        /// <param name="column">起始欄位X軸</param>
        /// <param name="row">起始欄位Y軸</param>
        /// <returns></returns>
        public static ExcelPackage SheetFormatter<T>(this ExcelPackage ep, int Quantity, ExcelSheetFormatter<T> source, int column = 1, int row = 1)
        {
            ep.Workbook.Worksheets[Quantity].Cells[row, column].LoadFromCollection(source.SheetData);
            return ep;
        }
    }
}