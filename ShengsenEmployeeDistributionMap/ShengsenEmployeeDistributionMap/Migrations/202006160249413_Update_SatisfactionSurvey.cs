namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_SatisfactionSurvey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SatisfactionSurvey", "Appraiser", c => c.String(maxLength: 30));
            AddColumn("dbo.SatisfactionSurvey", "AppraiseDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SatisfactionSurvey", "AppraiseDate");
            DropColumn("dbo.SatisfactionSurvey", "Appraiser");
        }
    }
}
