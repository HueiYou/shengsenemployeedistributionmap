// <auto-generated />
namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class updateTable1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updateTable1));
        
        string IMigrationMetadata.Id
        {
            get { return "201912201131505_updateTable1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
