﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace ShengsenEmployeeDistributionMap.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<EmployeesGroup> EmployeesGroup { get; set; }
        public virtual DbSet<LivingArea> LivingArea { get; set; }
        public virtual DbSet<StationedCompany> StationedCompany { get; set; }
        public virtual DbSet<StationedCompanyGroup> StationedCompanyGroup { get; set; }
        public virtual DbSet<SatisfactionSurvey> SatisfactionSurvey { get; set; }
        public virtual DbSet<Group> Group { get; set; }
    }
}