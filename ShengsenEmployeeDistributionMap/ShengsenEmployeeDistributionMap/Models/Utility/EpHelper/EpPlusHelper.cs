﻿using System.Linq;
using ShengsenEmployeeDistributionMap.Models.Utility.EpHelper.Model;
using OfficeOpenXml;

namespace ShengsenEmployeeDistributionMap.Models.Utility.EpHelper
{
    public class EpPlusHelper
    {
        public static byte[] ForceCreateSheet<T>(ExcelSheetFormatter<T> formatter)
        {
            using (var excel = new ExcelPackage())
            {
                var sheet = excel.Workbook.Worksheets.Add(formatter.SheetName);
                sheet.Cells["A1"].LoadFromCollection(formatter.SheetData, true, formatter.TableStyles);
                if (formatter.Title.Any())
                {
                    foreach (var item in formatter.Title.Select((value, i) => new { i, value }))
                    {
                        sheet.Cells[1, item.i].Value = item.value;
                        sheet.Column(item.i + 1).AutoFit();
                    }
                }

                if (formatter.Format.Any())
                {
                    foreach (var item in formatter.Format.Select((value, i) => new { i, value }))
                    {
                        sheet.Column(item.i + 1).Style.Numberformat.Format = item.value;
                    }
                }
                var bytes = excel.GetAsByteArray();
                return bytes;
            }
        }
    }
}