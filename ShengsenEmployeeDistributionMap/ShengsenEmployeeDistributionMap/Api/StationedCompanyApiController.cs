﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ShengsenEmployeeDistributionMap.Models;
using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.UniForm;

namespace ShengsenEmployeeDistributionMap.Api
{
    /// <summary>
    /// 駐點公司Api
    /// </summary>
    [Authorize]
    public class StationedCompanyApiController : ApiController
    {
        private StationedCompanyBLO _StationedCompanyBLO;
        public StationedCompanyApiController()
        {
            _StationedCompanyBLO = new StationedCompanyBLO();
        }

        /// <summary>
        /// 取得駐點公司過濾資料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public OutUniResult GetFilterData(string CompanySimpleName, string CompanyName = "", string CompanyAddress = "")
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _StationedCompanyBLO.GetFilterData(CompanySimpleName, CompanyName, CompanyAddress), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Delete([FromBody]StationedCompany item)
        {
            try
            {
                if (string.IsNullOrEmpty(item.Id))
                    return new HttpResponseMessage(HttpStatusCode.NotFound);

                int result = _StationedCompanyBLO.Delete(item);
                if (result > 0)
                    return new HttpResponseMessage(HttpStatusCode.OK);
                else
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("error:" + ex.Message)
                };
            }
        }

        /// <summary>
        /// 取得駐點公司下拉選單
        /// </summary>
        /// <returns></returns>
        public OutUniResult GetStationedCompanySelectListItems()
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _StationedCompanyBLO.GetStationedCompanySelectListItems(), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.OK, null, ex.Message);
            }
        }
    }
}