﻿using ShengsenEmployeeDistributionMap.Models.DAO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using ShengsenEmployeeDistributionMap.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace ShengsenEmployeeDistributionMap.Models.BLO
{
    public class GroupBLO
    {
        private readonly GroupDAO _groupDao;
        private readonly EmployeesGroupBLO _employeesGroupBLO;
        private readonly EmployeesBLO _employeesBLO;
        private readonly StationedCompanyGroupBLO _stationedCompanyGroupBLO;
        private readonly StationedCompanyBLO _stationedCompanyBLO;

        /// <summary>
        /// Constructor
        /// </summary>
        public GroupBLO()
        {
            _groupDao = new GroupDAO();
            _employeesBLO = new EmployeesBLO();
            _employeesGroupBLO = new EmployeesGroupBLO();
            _stationedCompanyBLO = new StationedCompanyBLO();
            _stationedCompanyGroupBLO = new StationedCompanyGroupBLO();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<Group> GetAll()
        {
            return _groupDao.GetAll();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(List<Group> item)
        {
            return _groupDao.Create(item);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(Group item)
        {
            return _groupDao.Update(item);
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(Group item)
        {
            return _groupDao.Delete(item);
        }

        /// <summary>
        /// 取得群組清單資料
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OutGroupViewModel> GetGroupList(InGroupSearchViewModel param)
        {
            #region get parameter

            IEnumerable<Group> groupEnumerable = GetAll();
            IEnumerable<StationedCompanyGroup> companyGroupEnumerable = _stationedCompanyGroupBLO.GetAll();

            #region Group Condition

            groupEnumerable = string.IsNullOrEmpty(param.Id) ? groupEnumerable : groupEnumerable.Where(x => x.Id == param.Id);
            groupEnumerable = string.IsNullOrEmpty(param.QueryString) ? groupEnumerable : groupEnumerable.Where(x => x.GroupName.Contains(param.QueryString));
            companyGroupEnumerable = string.IsNullOrEmpty(param.CompanyId) ? companyGroupEnumerable : companyGroupEnumerable.Where(x => x.StationedCompanyId == param.CompanyId);

            #endregion Group Condition

            List<Group> groupList = groupEnumerable.ToList();
            List<EmployeesGroup> employeeGroupList = _employeesGroupBLO.GetAll().ToList();
            List<Employees> employeeList = _employeesBLO.GetAll().ToList();
            List<StationedCompanyGroup> companyGroupList = companyGroupEnumerable.ToList();
            List<StationedCompany> companyList = _stationedCompanyBLO.GetAll().ToList();

            #endregion get parameter

            #region join

            List<OutGroupViewModel> result = groupList
                .Join(employeeGroupList,
                g => g.Id,
                eg => eg.GroupId,
                (g, eg) => new { group = g, empgroup = eg })
                .Join(employeeList,
                g => g.empgroup.EmployeeId,
                e => e.Id,
                (g, e) => new { g.group, g.empgroup, emp = e })
                .Join(companyGroupList,
                g => g.group.Id,
                cg => cg.GroupId,
                (g, cg) => new { g.group, g.empgroup, g.emp, companygroup = cg })
                .Join(companyList,
                g => g.companygroup.StationedCompanyId,
                c => c.Id,
                (g, c) => new { g.group, g.empgroup, g.emp, g.companygroup, company = c })
                .GroupBy(x => new { x.group.Id, x.group.GroupName, x.companygroup.StationedCompanyId, x.company.CompanySimpleName },
                (key, group) => new OutGroupViewModel
                {
                    Id = key.Id,
                    GroupName = key.GroupName,
                    StationedCompanyId = key.StationedCompanyId,
                    StationedCompanyName = key.CompanySimpleName,
                    Employees = group.Select(y => new OutGroupViewModel.EmployeeList
                    {
                        EmployeeId = y.empgroup.EmployeeId,
                        EmployeeName = y.emp.EmployeeName
                    }).OrderBy(x => x.EmployeeName).ToList()
                }).OrderBy(x => x.StationedCompanyId).ThenBy(x => x.GroupName).ToList();

            #endregion join

            return result;
        }

        /// <summary>
        /// 建立群組
        /// </summary>
        /// <param name="param"></param>
        public void CreateGroup(InGroupCreateViewModel param)
        {
            using (TransactionScope tx = new TransactionScope())
            {
                string groupId = Guid.NewGuid().ToString();

                #region 群組

                Create(new List<Group> {
                new Group
                {
                    Id = groupId,
                    GroupName = param.GroupName
                }});

                #endregion 群組

                #region 駐點公司群組

                _stationedCompanyGroupBLO.Create(new List<StationedCompanyGroup> {
                new StationedCompanyGroup{
                    Id = Guid.NewGuid().ToString(),
                    GroupId = groupId,
                    StationedCompanyId = param.StationedCompanyId
                }});

                #endregion 駐點公司群組

                #region 員工群組

                List<EmployeesGroup> empGroup = param.EmployeeIdList.Select(x => new EmployeesGroup
                {
                    Id = Guid.NewGuid().ToString(),
                    GroupId = groupId,
                    EmployeeId = x.Id
                }).ToList();

                //List<EmployeesGroup> empGroup = new List<EmployeesGroup>();
                //foreach (var employee in param.EmployeeIdList)
                //{
                //    empGroup.Add(new EmployeesGroup
                //    {
                //        Id = Guid.NewGuid().ToString(),
                //        GroupId = groupId,
                //        EmployeeId = employee.Id
                //    });
                //}
                _employeesGroupBLO.Create(empGroup);

                #endregion 員工群組

                tx.Complete();
            }
        }

        /// <summary>
        /// 更新群組
        /// </summary>
        /// <param name="param"></param>
        public void UpdateGroup(InGroupCreateViewModel param)
        {
            using (TransactionScope tx = new TransactionScope())
            {
                #region 更新群組資料

                Update(new Group { Id = param.Id, GroupName = param.GroupName });

                #endregion 更新群組資料

                #region 員工群組關聯檔資料

                // 資料庫既有資料
                List<EmployeesGroup> empGroup = _employeesGroupBLO.GetAll().Where(x => x.GroupId == param.Id).ToList();
                IEnumerable<string> tableEmpGroup = empGroup.Select(x => x.EmployeeId);
                // 畫面編輯資料
                IEnumerable<string> viewEmpGroup = param.EmployeeIdList.Select(x => x.Id);
                // 刪除
                IEnumerable<EmployeesGroup> deleteIdList = empGroup.Where(x => tableEmpGroup.Except(viewEmpGroup).Contains(x.EmployeeId));
                _employeesGroupBLO.Delete(deleteIdList);
                // 新增
                IEnumerable<EmployeesGroup> insertIdList = viewEmpGroup.Except(tableEmpGroup).Select(x => new EmployeesGroup
                {
                    Id = Guid.NewGuid().ToString(),
                    EmployeeId = x,
                    GroupId = param.Id
                });
                _employeesGroupBLO.Create(insertIdList);

                #endregion 員工群組關聯檔資料

                tx.Complete();
            }
        }

        /// <summary>
        /// 刪除群組
        /// </summary>
        /// <param name="param"></param>
        public void DeleteGroup(InGroupCreateViewModel param)
        {
            Delete(new Group { Id = param.Id });
        }
    }
}