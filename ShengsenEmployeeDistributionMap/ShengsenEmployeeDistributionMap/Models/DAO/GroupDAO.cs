﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShengsenEmployeeDistributionMap.Models.DAO
{
    public class GroupDAO : IDisposable
    {
        private readonly ApplicationDbContext db;

        /// <summary>
        /// Constructor
        /// </summary>
        public GroupDAO()
        {
            db = new ApplicationDbContext();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IQueryable<Group> GetAll()
        {
            return db.Group.AsNoTracking().AsQueryable();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(IEnumerable<Group> item)
        {
            db.Group.AddRange(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(Group item)
        {
            Group updateItem = db.Group.Find(item.Id);
            db.Entry(updateItem).CurrentValues.SetValues(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(Group item)
        {
            Group deleteItem = db.Group.Find(item.Id);
            db.Group.Remove(deleteItem);
            int result = db.SaveChanges();

            return result;
        }

        #region Dispose

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        #endregion Dispose
    }
}