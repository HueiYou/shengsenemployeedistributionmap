﻿using ShengsenEmployeeDistributionMap.Models.DAO;
using System.Collections.Generic;
using System.Linq;

namespace ShengsenEmployeeDistributionMap.Models.BLO
{
    public class StationedCompanyGroupBLO
    {
        private readonly StationedCompanyGroupDAO _companyGroupDao;

        public StationedCompanyGroupBLO()
        {
            _companyGroupDao = new StationedCompanyGroupDAO();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<StationedCompanyGroup> GetAll()
        {
            return _companyGroupDao.GetAll();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(List<StationedCompanyGroup> item)
        {
            return _companyGroupDao.Create(item);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(StationedCompanyGroup item)
        {
            return _companyGroupDao.Update(item);
        }
    }
}