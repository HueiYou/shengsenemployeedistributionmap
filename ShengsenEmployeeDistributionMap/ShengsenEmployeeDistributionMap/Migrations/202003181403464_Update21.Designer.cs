// <auto-generated />
namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Update21 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Update21));
        
        string IMigrationMetadata.Id
        {
            get { return "202003181403464_Update21"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
