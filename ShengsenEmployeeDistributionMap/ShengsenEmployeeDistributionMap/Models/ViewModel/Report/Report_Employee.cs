﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.Report
{
    /// <summary>
    /// 員工資訊匯出EXCEL報表
    /// </summary>
    public class Report_Employee
    {
        /// <summary>
        /// 類別 (來源) 名稱
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 類別 (來源)
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 技能
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// 員工姓名
        /// </summary>
        public string EmployeeName { get; set; }

        /// <summary>
        /// 性別名稱
        /// </summary>
        public string GenderName { get; set; }

        /// <summary>
        /// 性別
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// 出生年月日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 住所地址
        /// </summary>
        public string Residence { get; set; }

        /// <summary>
        /// 縣市名稱
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 縣市代碼
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 鄉、鎮市區名稱
        /// </summary>
        public string DistrictName { get; set; }

        /// <summary>
        /// 鄉、鎮市區代碼
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 學歷
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// 經歷
        /// </summary>
        public string WorkExperience { get; set; }

        /// <summary>
        /// 技能描述
        /// </summary>
        public string SkillDescription { get; set; }

        /// <summary>
        /// 四型人格
        /// </summary>
        public string Personality { get; set; }

        /// <summary>
        /// 開始工作日期
        /// 工程師總年資
        /// </summary>
        public DateTime StartWorkDate { get; set; }

        /// <summary>
        /// 聖森到職日期
        /// </summary>
        public DateTime? OnBoardDate { get; set; }

        /// <summary>
        /// 駐點公司簡稱
        /// </summary>
        public string CompanySimpleName { get; set; }

        /// <summary>
        /// 駐點公司代碼
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 駐點日期
        /// </summary>
        public DateTime? LocationedDate { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        public string Remark { get; set; }
    }
}