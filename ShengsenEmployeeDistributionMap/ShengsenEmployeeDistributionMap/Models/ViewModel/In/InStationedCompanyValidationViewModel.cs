﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    /// <summary>
    /// 駐點公司驗證用ViewModel
    /// </summary>
    public class InStationedCompanyValidationViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(50, ErrorMessage = "{0} 不可超過50字元")]
        [Display(Name = "駐點公司簡稱")]
        public string CompanySimpleName { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(50, ErrorMessage = "{0} 不可超過50字元")]
        [Display(Name = "駐點公司名稱")]
        public string CompanyName { get; set; }

        [StringLength(150, ErrorMessage = "{0} 不可超過150字元")]
        [Display(Name = "駐點公司地址")]
        public string CompanyAddress { get; set; }
    }
}