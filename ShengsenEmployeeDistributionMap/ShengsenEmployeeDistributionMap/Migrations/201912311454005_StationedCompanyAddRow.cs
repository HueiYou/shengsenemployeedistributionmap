namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StationedCompanyAddRow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StationedCompany", "CompanySimpleName", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StationedCompany", "CompanySimpleName");
        }
    }
}
