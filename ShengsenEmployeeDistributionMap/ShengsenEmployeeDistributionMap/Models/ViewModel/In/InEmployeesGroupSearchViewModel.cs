﻿namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    public class InEmployeesGroupSearchViewModel
    {
        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 群組編號
        /// </summary>
        public string GroupId { get; set; }
    }
}