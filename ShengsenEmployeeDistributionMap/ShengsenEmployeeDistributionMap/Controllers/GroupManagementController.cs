﻿using System.Web.Mvc;

namespace ShengsenEmployeeDistributionMap.Controllers
{
    /// <summary>
    /// 員工群組
    /// </summary>
    [Authorize]
    public class GroupManagementController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }
    }
}