﻿using System;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    public class InSatisfactionSurveyPageViewModel
    {
        /// <summary>
        /// 編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 員工編號
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// 員工姓名
        /// </summary>
        public string EmployeeName { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 駐點公司名稱
        /// </summary>
        public string StationedName { get; set; }

        /// <summary>
        /// 評估期間
        /// </summary>
        public DateTime? EvaluateDate { get; set; }

        /// <summary>
        /// 服務熱忱分數
        /// </summary>
        public int EnthusiasmScore { get; set; }

        /// <summary>
        /// 服務熱忱說明(1、2、5分必填)
        /// </summary>
        public string EnthusiasmDesc { get; set; }

        /// <summary>
        /// 溝通協調分數
        /// </summary>
        public int CommunicationScore { get; set; }

        /// <summary>
        /// 溝通協調說明(1、2、5分必填)
        /// </summary>
        public string CommunicationDesc { get; set; }

        /// <summary>
        /// 解決問題分數
        /// </summary>
        public int ResolveScore { get; set; }

        /// <summary>
        /// 解決問題說明(1、2、5分必填)
        /// </summary>
        public string ResolveDesc { get; set; }

        /// <summary>
        /// 專業知識分數
        /// </summary>
        public int ProfessionScore { get; set; }

        /// <summary>
        /// 專業知識說明(1、2、5分必填)
        /// </summary>
        public string ProfessionDesc { get; set; }

        /// <summary>
        /// 時間控制分數
        /// </summary>
        public int TimeControlScore { get; set; }

        /// <summary>
        /// 時間控制說明(1、2、5分必填)
        /// </summary>
        public string TimeControlDesc { get; set; }

        /// <summary>
        /// 評核人員
        /// </summary>
        public string Appraiser { get; set; }

        /// <summary>
        /// 評核日期
        /// </summary>
        public DateTime AppraiseDate { get; set; }

        /// <summary>
        /// 評語與建議事項
        /// </summary>
        public string Comment { get; set; }
    }
}