namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "Gender", c => c.Int(nullable: false));
            AddColumn("dbo.Employees", "SkillDescription", c => c.String());
            AddColumn("dbo.Employees", "Remark", c => c.String());
            AddColumn("dbo.Employees", "CreateDate", c => c.DateTime(storeType: "date"));
            AddColumn("dbo.Employees", "OnBoardDate", c => c.DateTime(storeType: "date"));
            AddColumn("dbo.Employees", "LocationedDate", c => c.DateTime(storeType: "date"));
            AddColumn("dbo.Employees", "Birthday", c => c.DateTime(storeType: "date"));
            AlterColumn("dbo.Employees", "Personality", c => c.String(maxLength: 20));
            AlterColumn("dbo.Employees", "Residence", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employees", "Residence", c => c.String(nullable: false));
            AlterColumn("dbo.Employees", "Personality", c => c.String(nullable: false, maxLength: 20));
            DropColumn("dbo.Employees", "Birthday");
            DropColumn("dbo.Employees", "LocationedDate");
            DropColumn("dbo.Employees", "OnBoardDate");
            DropColumn("dbo.Employees", "CreateDate");
            DropColumn("dbo.Employees", "Remark");
            DropColumn("dbo.Employees", "SkillDescription");
            DropColumn("dbo.Employees", "Gender");
        }
    }
}
