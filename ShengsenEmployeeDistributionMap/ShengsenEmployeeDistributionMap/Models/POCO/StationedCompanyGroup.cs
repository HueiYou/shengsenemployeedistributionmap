﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShengsenEmployeeDistributionMap.Models
{
    [Table("StationedCompanyGroup")]
    public partial class StationedCompanyGroup
    {
        /// <summary>
        /// 駐點公司群組編號
        /// </summary>
        [Key]
        [StringLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 群組編號
        /// </summary>
        [Required]
        [StringLength(50)]
        [ForeignKey("Group")]
        public string GroupId { get; set; }

        public Group Group { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        [Required]
        [StringLength(50)]
        public string StationedCompanyId { get; set; }
    }
}