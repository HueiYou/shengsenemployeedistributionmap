﻿//using MailKit.Net.Smtp;
//using MailKit.Security;
//using MimeKit;
using System.Configuration;
using System.Net.Mail;

namespace ReportSystem.Models
{
    public static class SMTPHelper
    {
        public static void SendMail(MailMessage Message)
        {
            // login user
            string MailUser = ConfigurationManager.AppSettings["MailAddress"];
            // login passward
            string MailPw = ConfigurationManager.AppSettings["Password"];
            // mail from
            string MailAddress = ConfigurationManager.AppSettings["MailAddress"];
            // mail from display name
            string MailAddressName = ConfigurationManager.AppSettings["UserName"];
            // Host FQDN
            string SmtpHost = ConfigurationManager.AppSettings["SMTPServer"];
            // Port
            int.TryParse(ConfigurationManager.AppSettings["MailPort"], out int SmtpPort);

            // Service
            SmtpClient MySmtp = new SmtpClient(SmtpHost, SmtpPort);
            //Login
            MySmtp.Credentials = new System.Net.NetworkCredential(MailUser, MailPw);
            //open SSL
            MySmtp.EnableSsl = true;
            MySmtp.Send(Message);
            Message.Dispose();
        }
    }
}
