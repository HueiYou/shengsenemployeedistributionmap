﻿using OfficeOpenXml;
using ReportSystem.Models;
using ShengsenEmployeeDistributionMap.Models.DAO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using ShengsenEmployeeDistributionMap.Models.ViewModel.Out;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Net.Mail;
using ShengsenEmployeeDistributionMap.Models.Utility.EpHelper.Model;
using ShengsenEmployeeDistributionMap.Models.ViewModel.Report;
using ShengsenEmployeeDistributionMap.Models.Utility.EpHelper;

namespace ShengsenEmployeeDistributionMap.Models.BLO
{
    /// <summary>
    /// 員工BLO
    /// </summary>
    public class EmployeesBLO
    {
        private readonly EmployeesDAO _EmployeesDAO;
        private readonly LivingAreaBLO _LivingAreaBLO;
        private readonly StationedCompanyBLO _StationedCompanyBLO;
        private readonly EmployeesGroupBLO _employeesGroupBLO;

        public EmployeesBLO()
        {
            _EmployeesDAO = new EmployeesDAO();
            _LivingAreaBLO = new LivingAreaBLO();
            _StationedCompanyBLO = new StationedCompanyBLO();
            _employeesGroupBLO = new EmployeesGroupBLO();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<Employees> GetAll()
        {
            return _EmployeesDAO.GetAll();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(Employees item)
        {
            int result = _EmployeesDAO.Create(item);

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(Employees item)
        {
            int result = _EmployeesDAO.Update(item);

            return result;
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(Employees item)
        {
            int result = _EmployeesDAO.Delete(item);

            return result;
        }

        /// <summary>
        /// 取得員工資訊所有資料
        /// </summary>
        /// <param name="searchModel">搜尋條件</param>
        /// <returns></returns>
        public IEnumerable<OutEmployeesViewModel> GetEmployeesViewModel(InEmployeesSearchViewModel searchModel)
        {
            #region 取得各資料的List
            List<Employees> eList = _EmployeesDAO.GetAll().ToList();
            List<LivingArea> lList = _LivingAreaBLO.GetAll().ToList();
            List<StationedCompany> sList = _StationedCompanyBLO.GetAll().ToList();
            DateTime dt = DateTime.Now;
            #endregion

            #region 組成員工資訊
            var result = (from e in eList
                          join l in lList on new { e?.CityCode, e?.DistrictCode } equals new { l.CityCode, l.DistrictCode } into el
                          from l in el.DefaultIfEmpty()
                          join s in sList on new { Id = e?.CompanyId } equals new { s.Id } into es
                          from s in es.DefaultIfEmpty()
                          select new OutEmployeesViewModel()
                          {
                              Id = e.Id,
                              EmployeeName = e.EmployeeName,
                              CityCode = l == null ? "" : l.CityCode,
                              CityName = l == null ? "" : l.CityName,
                              DistrictCode = l == null ? "" : l.DistrictCode,
                              DistrictName = l == null ? "" : l.DistrictName,
                              CompanyId = s == null ? "" : s.Id,
                              CompanySimpleName = s == null ? "" : s.CompanySimpleName,
                              Department = e.Department,
                              Category = e.Category,
                              CategoryName = e.Category == 1 ? "104人力銀行" : (e.Category == 2 ? "在職員工" : "離職員工"),
                              Personality = e.Personality,
                              Education = e.Education,
                              WorkExperience = e.WorkExperience,
                              //計算年資(月份)
                              TotalWorkMonths = (dt.Year * 12 + dt.Month) - (e.StartWorkDate.Year * 12 + e.StartWorkDate.Month) + (dt.Year <= e.StartWorkDate.Year ? 0 : dt.Month > e.StartWorkDate.Month ? 1 : (dt.Month == e.StartWorkDate.Month && dt.Day >= e.StartWorkDate.Day) ? 1 : 0),
                              Residence = e.Residence,
                          })
                          .OrderBy(x => x.CompanySimpleName)
                          .ThenBy(x => x.Department)
                          .AsEnumerable();
            #endregion

            #region 搜尋條件過濾，放在上面的話Left join會有問題
            if (searchModel != null)
            {
                //員工姓名
                if (!string.IsNullOrEmpty(searchModel.EmployeeName))
                    result = result.Where(x => x.EmployeeName.Contains(searchModel.EmployeeName));

                //技能
                if (!string.IsNullOrEmpty(searchModel.Department))
                    result = result.Where(x => x.Department.Contains(searchModel.Department));

                //縣市
                if (!string.IsNullOrEmpty(searchModel.CityCode))
                    result = result.Where(x => x.CityCode.Equals(searchModel.CityCode));

                //鄉鎮市區
                if (!string.IsNullOrEmpty(searchModel.DistrictCode))
                    result = result.Where(x => x.DistrictCode.Equals(searchModel.DistrictCode));

                //駐點公司編號
                if (!string.IsNullOrEmpty(searchModel.CompanyId))
                    result = result.Where(x => x.CompanyId.Equals(searchModel.CompanyId));

                //駐點公司
                if (!string.IsNullOrEmpty(searchModel.CompanySimpleName))
                    result = result.Where(x => x.CompanySimpleName.Equals(searchModel.CompanySimpleName));

                //類別(來源)
                if (searchModel.Category != null)
                    result = result.Where(x => x.Category.Equals(searchModel.Category));

                //四型人格
                if (!string.IsNullOrEmpty(searchModel.Personality))
                    result = result.Where(x => x.Personality.Contains(searchModel.Personality));

                //學歷/學位
                if (!string.IsNullOrEmpty(searchModel.Education))
                    result = result.Where(x => x.Education.Contains(searchModel.Education));

                //經歷
                if (!string.IsNullOrEmpty(searchModel.WorkExperience))
                    result = result.Where(x => x.WorkExperience.Contains(searchModel.WorkExperience));

                //總年資(月份)起訖
                if (searchModel.TotalWorkMonthsStart != null)
                    result = result.Where(x => x.TotalWorkMonths >= searchModel.TotalWorkMonthsStart);
                if (searchModel.TotalWorkMonthsEnd != null)
                    result = result.Where(x => x.TotalWorkMonths <= searchModel.TotalWorkMonthsEnd);

                //住所地址
                if (!string.IsNullOrEmpty(searchModel.Residence))
                    result = result.Where(x => x.Residence.Contains(searchModel.Residence));
            }
            #endregion

            if (result == null)
                return new List<OutEmployeesViewModel>();
            else
                return result;
        }

        /// <summary>
        /// 取得員工群組資料
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        public IEnumerable<OutEmployeesGroupListViewModel> GetEmployeesGroupList(InEmployeesGroupSearchViewModel searchModel)
        {
            List<Employees> eList = _EmployeesDAO.GetAll().Where(x => x.CompanyId == searchModel.CompanyId).ToList();
            List<EmployeesGroup> egList = _employeesGroupBLO.GetAll().ToList();

            IEnumerable<OutEmployeesGroupListViewModel> result = eList.GroupJoin(egList,
                e => e.Id,
                eg => eg.EmployeeId,
                (e, eg) => new { employee = e, empGroup = eg })
                .SelectMany(x => x.empGroup.DefaultIfEmpty(), (e, eg) => new OutEmployeesGroupListViewModel
                {
                    Id = e.employee.Id,
                    EmployeeName = e.employee.EmployeeName,
                    GroupId = eg != null ? eg.GroupId : string.Empty
                })
                .Where(x => string.IsNullOrEmpty(x.GroupId) || x.GroupId == searchModel.GroupId);

            return result;
        }

        #region 匯入資料
        /// <summary>
        /// 匯入員工資料
        /// </summary>
        /// <param name="importFile"></param>
        /// <returns>是否新增成功</returns>
        public bool ImportEmployeesData(string filePath)
        {
            try
            {
                //匯入結果
                List<Employees> dataItems = new List<Employees>();
                //檢查匯入的資料
                string totalMsg = CheckImportEmployeesData(filePath, ref dataItems);
                if (!string.IsNullOrEmpty(totalMsg))
                {
                    throw new Exception(totalMsg);
                }

                //批次新增員工資料
                _EmployeesDAO.Creates(dataItems);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("匯入資料錯誤：\n" + ex.Message);
            }
        }

        /// <summary>
        /// 檢查匯入員工資料是否符合格式
        /// </summary>
        /// <param name="importFile">匯入檔案</param>
        /// <returns>錯誤訊息</returns>
        public string CheckImportEmployeesData(string filePath, ref List<Employees> dataItems)
        {
            try
            {
                //取得檔案
                FileInfo fileInfo = new FileInfo(filePath);
                //所有錯誤
                StringBuilder totalMsg = new StringBuilder("");
                //每列錯誤
                StringBuilder rowMsg = new StringBuilder("");

                //檢查檔案是否存在
                if (fileInfo.Exists)
                {
                    //讀取Excel資料到對應Model
                    using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
                    {
                        #region 取得excel資料
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets[1]; //取得Sheet1
                        int startRowIndex = sheet.Dimension.Start.Row;//起始列
                        int startColumn = sheet.Dimension.Start.Column;//起始欄
                        int endRowIndex = sheet.Dimension.End.Row;//結束列
                        int endColumn = sheet.Dimension.End.Column;//結束欄
                        bool isHeader = true;   //是否有標題列
                        int emptyCount = 0; //連續空行數
                        List<LivingArea> lList = _LivingAreaBLO.GetAll().AsNoTracking().ToList();   //縣市鄉鎮市區資料
                        List<StationedCompany> sList = _StationedCompanyBLO.GetAll().AsNoTracking().ToList();   //駐點公司資料
                        #endregion

                        if (isHeader)//有包含標題
                        {
                            startRowIndex += 1;
                        }

                        //組成資料
                        for (int currentRow = startRowIndex; currentRow <= endRowIndex; currentRow++)
                        {
                            rowMsg.Clear(); //清空之前的錯誤訊息
                            //抓出當前列的資料範圍
                            ExcelRange range = sheet.Cells[currentRow, startColumn, currentRow, endColumn];

                            #region 讀取防呆
                            //全部儲存格是完全空白時則跳過
                            if (range.Any(c => !string.IsNullOrEmpty(c.Text)) == false)
                            {
                                emptyCount++;
                                continue;
                            }
                            else
                                emptyCount = 0;

                            //連續空行>5就跳出迴圈
                            if (emptyCount >= 5)
                                break;
                            #endregion

                            #region 驗證各個欄位資料是否正確並組成所有錯誤訊息
                            //類別代碼
                            int Category = 0;
                            if (sheet.Cells[currentRow, 2].Value == null)
                                rowMsg.AppendFormat("類別代碼為必填欄位、");
                            else if (int.TryParse(sheet.Cells[currentRow, 2].Value.ToString(), out Category) == false)
                                rowMsg.AppendFormat("類別代碼須為數值、");
                            else if (Category <= 0 || Category > 3)
                                rowMsg.AppendFormat("類別代碼不存在、");

                            //技能
                            string Department = sheet.Cells[currentRow, 3].Value?.ToString().ToUpper();
                            if (string.IsNullOrEmpty(Department))
                                rowMsg.AppendFormat("技能為必填欄位、");
                            else if (Department != "JAVA" && Department != ".NET")
                                rowMsg.AppendFormat("技能不存在、");

                            //員工姓名
                            string EmployeeName = sheet.Cells[currentRow, 4].Value?.ToString();
                            if (string.IsNullOrEmpty(EmployeeName))
                                rowMsg.AppendFormat("員工姓名為必填欄位、");

                            //性別
                            string Gender = sheet.Cells[currentRow, 6].Value?.ToString();
                            if (string.IsNullOrEmpty(Gender))
                                rowMsg.AppendFormat("性別為必填欄位、");
                            else if (Gender != "0" && Gender != "1")
                                rowMsg.AppendFormat("性別不存在、");

                            //出生年月日
                            DateTime? CheckBirthday = null;
                            if (sheet.Cells[currentRow, 7].Value != null)
                            {
                                CheckBirthday = ConvertExcelDateTimeIntoCLRDateTime(sheet.Cells[currentRow, 7].Value);
                                if(!CheckBirthday.HasValue)
                                    rowMsg.AppendFormat("出生年月日格式錯誤、");
                            }

                            //縣市代碼
                            string CheckCityCode = sheet.Cells[currentRow, 10].Value?.ToString();
                            string CityCode = lList.FirstOrDefault(x => x.CityCode == CheckCityCode)?.CityCode;
                            if (string.IsNullOrEmpty(CheckCityCode))
                                rowMsg.AppendFormat("縣市代碼為必填欄位、");
                            else if (CityCode == null)
                                rowMsg.AppendFormat("縣市代碼不存在、");

                            //鄉鎮市區代碼
                            string CheckDistrictCode = sheet.Cells[currentRow, 12].Value?.ToString();
                            string DistrictCode = lList.FirstOrDefault(x => x.CityCode == CityCode && x.DistrictCode == CheckDistrictCode)?.DistrictCode;
                            if (string.IsNullOrEmpty(CheckDistrictCode))
                                rowMsg.AppendFormat("鄉鎮市區代碼為必填欄位、");
                            else if (DistrictCode == null)
                                rowMsg.AppendFormat("鄉鎮市區代碼不存在、");

                            //學歷
                            string Education = sheet.Cells[currentRow, 13].Value?.ToString();
                            if (string.IsNullOrEmpty(Education))
                                rowMsg.AppendFormat("學歷為必填欄位、");

                            //開始工作日期
                            DateTime? CheckStartWorkDate = null;
                            if (sheet.Cells[currentRow, 17].Value == null)
                                rowMsg.AppendFormat("開始工作日期為必填欄位、");
                            else
                            {
                                CheckStartWorkDate = ConvertExcelDateTimeIntoCLRDateTime(sheet.Cells[currentRow, 17].Value);
                                if (!CheckStartWorkDate.HasValue)
                                    rowMsg.AppendFormat("開始工作日期格式錯誤、");
                            }

                            //聖森到職日期
                            DateTime? CheckOnBoardDate = null;
                            if (sheet.Cells[currentRow, 18].Value != null)
                            {
                                CheckOnBoardDate = ConvertExcelDateTimeIntoCLRDateTime(sheet.Cells[currentRow, 18].Value);
                                if (!CheckOnBoardDate.HasValue)
                                    rowMsg.AppendFormat("聖森到職日期格式錯誤、");
                            }

                            //公司簡稱
                            string CompanyId = sheet.Cells[currentRow, 20].Value?.ToString();
                            string CheckCompanyId = sList.FirstOrDefault(x => x.Id == CompanyId)?.Id;
                            if (!string.IsNullOrEmpty(CompanyId) && CheckCompanyId == null)
                                rowMsg.AppendFormat("駐點公司不存在、");

                            //駐點日期
                            DateTime? CheckLocationedDate = null;
                            if (sheet.Cells[currentRow, 21].Value != null)
                            {
                                CheckLocationedDate = ConvertExcelDateTimeIntoCLRDateTime(sheet.Cells[currentRow, 21].Value);
                                if (!CheckLocationedDate.HasValue)
                                    rowMsg.AppendFormat("駐點日期格式錯誤、");
                            }

                            //組成錯誤訊息
                            if (rowMsg.ToString() != "")
                                totalMsg.AppendFormat("第{0}列：{1}\n", currentRow, rowMsg.ToString().TrimEnd('、'));
                            #endregion

                            //都沒錯誤的話就順便組好資料
                            if (totalMsg.ToString() == "")
                            {
                                #region 塞值到Model
                                var dataItem = new Employees();
                                dataItem.Id = Guid.NewGuid().ToString();
                                dataItem.Category = Category; //存類別代碼
                                dataItem.Department = Department;  //技能
                                dataItem.EmployeeName = EmployeeName;    //員工姓名
                                dataItem.Gender = int.Parse(Gender);   //性別
                                dataItem.Birthday = CheckBirthday.HasValue ? CheckBirthday.Value : (DateTime?)null;   //非必填，出生年月日
                                dataItem.Residence = sheet.Cells[currentRow, 8].Value?.ToString();   //非必填，住所地址
                                dataItem.CityCode = CityCode;    //存縣市代碼
                                dataItem.DistrictCode = DistrictCode;    //存鄉鎮市區代碼
                                dataItem.Education = Education;   //學歷
                                dataItem.WorkExperience = sheet.Cells[currentRow, 14].Value?.ToString();  //非必填，經歷
                                dataItem.SkillDescription = sheet.Cells[currentRow, 15].Value?.ToString();   //非必填，技能描述
                                dataItem.Personality = sheet.Cells[currentRow, 16].Value?.ToString(); //非必填，四型人格
                                dataItem.StartWorkDate = CheckStartWorkDate.Value;  //開始工作日期
                                dataItem.OnBoardDate = CheckOnBoardDate.HasValue ? CheckOnBoardDate.Value : (DateTime?)null;  //非必填，聖森到職日期
                                dataItem.CompanyId = CompanyId;  //非必填，存公司簡稱
                                dataItem.LocationedDate = CheckLocationedDate.HasValue ? CheckLocationedDate.Value : (DateTime?)null;  //非必填，駐點日期
                                dataItem.Remark = sheet.Cells[currentRow, 22].Value?.ToString(); //非必填，備註
                                dataItems.Add(dataItem);
                                #endregion
                            }
                        }
                    }

                    return totalMsg.ToString();
                }
                else
                {
                    return "檔案不存在";
                }
            }
            catch (Exception ex)
            {
                return "檔案發生錯誤";
            }
        }

        /// <summary>
        /// object Convert DateTime
        /// </summary>
        /// <param name="value">傳入值</param>
        /// <returns>日期</returns>
        private DateTime? ConvertExcelDateTimeIntoCLRDateTime(object value)
        {
            try
            {
                if (value is DateTime)
                {
                    return DateTime.Parse(value.ToString());
                }
                else
                {
                    string dt = DateTime.FromOADate(Convert.ToInt32(value)).ToString("d");
                    return DateTime.Parse(dt);
                }
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 匯入報表範本 (每次下載更新駐點公司)
        /// </summary>
        /// <param name="filePath">範本路徑</param>
        /// <returns></returns>
        public byte[] ExportImportTemplate(string filePath)
        {
            List<StationedCompany> stationedCompany = this._StationedCompanyBLO.GetAll().ToList();

            using (var excel = new ExcelPackage(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                List<Report_StationedCompany> stationedList = stationedCompany.Select(x => new Report_StationedCompany
                {
                    Id = x.Id,
                    CompanySimpleName = x.CompanySimpleName
                }).ToList();
                ExcelSheetFormatter<Report_StationedCompany> stationedData = new ExcelSheetFormatter<Report_StationedCompany> { SheetData = stationedList };
                excel.SheetFormatter(2, stationedData, 54, 2);

                return excel.GetAsByteArray();
            }
        }
        #endregion


        #region 匯出資料
        /// <summary>
        /// 匯出員工資訊EXCEL
        /// </summary>
        /// <param name="filePath">匯出範本檔名</param>
        /// <returns>匯出二進位資料</returns>
        public byte[] ExportEmployeeData(string filePath)
        {
            #region 取得各資料的List
            List<Employees> eList = _EmployeesDAO.GetAll().ToList();
            List<LivingArea> lList = _LivingAreaBLO.GetAll().ToList();
            List<StationedCompany> sList = _StationedCompanyBLO.GetAll().ToList();
            DateTime dt = DateTime.Now;
            #endregion

            #region 取得相關資料
            List<Report_Employee> employeeContent = (from e in eList
                                                     join s in sList on new { Id = e.CompanyId } equals new { Id = s.Id } into es
                                                     from s in es.DefaultIfEmpty()
                                                     join l in lList on new { CityCode = e.CityCode, DistrictCode = e.DistrictCode } equals new { CityCode = l.CityCode, DistrictCode = l.DistrictCode }
                                                     select new Report_Employee()
                                                     {
                                                         CategoryName = e.Category == 0 ? "" : (e.Category == 1 ? "104人力銀行" : (e.Category == 2 ? "在職員工" : "離職員工")),
                                                         Category = e.Category,
                                                         Department = e.Department,
                                                         EmployeeName = e.EmployeeName,
                                                         GenderName = e.Gender == 0 ? "男" : "女",
                                                         Gender = e.Gender,
                                                         Birthday = e.Birthday,
                                                         Residence = e.Residence,
                                                         CityName = l.CityName,
                                                         CityCode = e.CityCode,
                                                         DistrictName = l.DistrictName,
                                                         DistrictCode = e.DistrictCode,
                                                         Education = e.Education,
                                                         WorkExperience = e.WorkExperience,
                                                         SkillDescription = e.SkillDescription,
                                                         Personality = e.Personality,
                                                         StartWorkDate = e.StartWorkDate,
                                                         OnBoardDate = e.OnBoardDate,
                                                         CompanySimpleName = s?.CompanySimpleName,
                                                         CompanyId = s?.Id,
                                                         LocationedDate = e.LocationedDate,
                                                         Remark = e.Remark
                                                     }).ToList();
            #endregion

            #region 寫入excel
            using (var excel = new ExcelPackage(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                // 員工資訊
                ExcelSheetFormatter<Report_Employee> employeeData = new ExcelSheetFormatter<Report_Employee> { SheetData = employeeContent };
                excel.SheetFormatter(1, employeeData, 1, 2);
                // 駐點公司
                List<Report_StationedCompany> stationedList = sList.Select(x => new Report_StationedCompany
                {
                    Id = x.Id,
                    CompanySimpleName = x.CompanySimpleName
                }).ToList();
                ExcelSheetFormatter<Report_StationedCompany> stationedData = new ExcelSheetFormatter<Report_StationedCompany> { SheetData = stationedList };
                excel.SheetFormatter(2, stationedData, 54, 2);

                return excel.GetAsByteArray();
            }
            #endregion
        }
        #endregion

        /// <summary>
        /// Email寄送基本資料連結
        /// </summary>
        /// <param name="MailString"></param>
        /// <returns></returns>
        public bool MailGenerator(string MailString)
        {
            try
            {
                string[] MailList = MailString.Split(',');

                foreach (string mail in MailList)
                {
                    #region Mime assignment

                    MailMessage Message = new MailMessage();
                    //發信Email，顯示的名稱
                    Message.From = new MailAddress("mickchenforjob@gmail.com", "聖森雲端科技股份有限公司");
                    //收信者Email
                    Message.To.Add(mail);
                    //設定優先權
                    Message.Priority = MailPriority.Normal;
                    //標題
                    Message.Subject = "聖森雲端科技股份有限公司履歷填寫網址";
                    //內文
                    Message.Body = $@"聖森雲端科技股份有限公司履歷填寫網址：
{ConfigurationManager.AppSettings["DefaultResumeAddress"]}{Guid.NewGuid()}";
                    //內容使用html
                    Message.IsBodyHtml = true;

                    SMTPHelper.SendMail(Message);

                    #endregion
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}