﻿using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.DMO
{
    /// <summary>
    /// 員工DMO
    /// </summary>
    public static class EmployeesDMO
    {
        /// <summary>
        /// 將POCO轉成ViewModel
        /// </summary>
        /// <param name="employees"></param>
        /// <returns></returns>
        public static InEmployeesValidationViewModel ToInEmployeesValidationViewModel(this Employees employees)
        {
            InEmployeesValidationViewModel result = new InEmployeesValidationViewModel()
            {
                Id = employees.Id,
                EmployeeName = employees.EmployeeName,
                CityCode = employees.CityCode,
                DistrictCode = employees.DistrictCode,
                CompanyId = employees.CompanyId,
                Department = employees.Department,
                Category = employees.Category,
                Personality = employees.Personality,
                Education = employees.Education,
                Residence = employees.Residence,
                StartWorkDate = employees.StartWorkDate,
                StartWorkDateString = employees.StartWorkDate.ToShortDateString(),
                WorkExperience = employees.WorkExperience,
                Gender = employees.Gender,
                SkillDescription = employees.SkillDescription,
                Remark = employees.Remark,
                CreateDate = employees.CreateDate,
                OnBoardDate = employees.OnBoardDate,
                LocationedDate = employees.LocationedDate,
                Birthday = employees.Birthday,
                OnBoardDateString = employees.OnBoardDate?.ToShortDateString(),
                LocationedDateString = employees.LocationedDate?.ToShortDateString(),
                BirthdayString = employees.Birthday?.ToShortDateString(),
            };

            return result;
        }

        /// <summary>
        /// 將ViewModel轉成POCO
        /// </summary>
        /// <param name="employees"></param>
        /// <returns></returns>
        public static Employees ToEmployees(this InEmployeesValidationViewModel employees)
        {
            Employees result = new Employees()
            {
                Id = employees.Id,
                EmployeeName = employees.EmployeeName,
                CityCode = employees.CityCode,
                DistrictCode = employees.DistrictCode,
                CompanyId = employees.CompanyId,
                Department = employees.Department,
                Category = employees.Category,
                Personality = employees.Personality,
                Education = employees.Education,
                Residence = employees.Residence,
                StartWorkDate = employees.StartWorkDate,
                WorkExperience = employees.WorkExperience,
                Gender = employees.Gender,
                SkillDescription = employees.SkillDescription,
                Remark = employees.Remark,
                CreateDate = employees.CreateDate,
                OnBoardDate = employees.OnBoardDate,
                LocationedDate = employees.LocationedDate,
                Birthday = employees.Birthday
            };

            return result;
        }
    }
}