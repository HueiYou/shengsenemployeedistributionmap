﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShengsenEmployeeDistributionMap.Models
{
    [Table("SatisfactionSurvey")]
    /// <summary>
    /// 滿意度調查
    /// </summary>
    public class SatisfactionSurvey
    {
        /// <summary>
        /// 編號
        /// </summary>
        [Key]
        [StringLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 員工編號
        /// </summary>
        [Required]
        [StringLength(50)]
        public string EmployeeId { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        [Required]
        [StringLength(50)]
        public string StationedCompanyId { get; set; }

        /// <summary>
        /// 評估期間
        /// </summary>
        public DateTime? EvaluateDate { get; set; }

        /// <summary>
        /// 服務熱忱分數
        /// </summary>
        [Required]
        public int EnthusiasmScore { get; set; }

        /// <summary>
        /// 服務熱忱說明(1、2、5分必填)
        /// </summary>
        [StringLength(100)]
        public string EnthusiasmDesc { get; set; }

        /// <summary>
        /// 溝通協調分數
        /// </summary>
        [Required]
        public int CommunicationScore { get; set; }

        /// <summary>
        /// 溝通協調說明(1、2、5分必填)
        /// </summary>
        [StringLength(100)]
        public string CommunicationDesc { get; set; }

        /// <summary>
        /// 解決問題分數
        /// </summary>
        [Required]
        public int ResolveScore { get; set; }

        /// <summary>
        /// 解決問題說明(1、2、5分必填)
        /// </summary>
        [StringLength(100)]
        public string ResolveDesc { get; set; }

        /// <summary>
        /// 專業知識分數
        /// </summary>
        [Required]
        public int ProfessionScore { get; set; }

        /// <summary>
        /// 專業知識說明(1、2、5分必填)
        /// </summary>
        [StringLength(100)]
        public string ProfessionDesc { get; set; }

        /// <summary>
        /// 時間控制分數
        /// </summary>
        [Required]
        public int TimeControlScore { get; set; }

        /// <summary>
        /// 時間控制說明(1、2、5分必填)
        /// </summary>
        [StringLength(100)]
        public string TimeControlDesc { get; set; }

        /// <summary>
        /// 評核人員
        /// </summary>
        [StringLength(30)]
        public string Appraiser { get; set; }

        /// <summary>
        /// 評核日期
        /// </summary>
        public DateTime? AppraiseDate { get; set; }

        /// <summary>
        /// 評語與建議事項
        /// </summary>
        [StringLength(200)]
        public string Comment { get; set; }
    }
}