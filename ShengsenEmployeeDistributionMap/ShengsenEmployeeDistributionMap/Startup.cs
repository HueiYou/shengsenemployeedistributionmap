﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShengsenEmployeeDistributionMap.Startup))]
namespace ShengsenEmployeeDistributionMap
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
