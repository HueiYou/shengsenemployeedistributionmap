﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShengsenEmployeeDistributionMap.Models.DAO
{
    public class StationedCompanyGroupDAO : IDisposable
    {
        private readonly ApplicationDbContext db;

        /// <summary>
        /// Constructor
        /// </summary>
        public StationedCompanyGroupDAO()
        {
            db = new ApplicationDbContext();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IQueryable<StationedCompanyGroup> GetAll()
        {
            return db.StationedCompanyGroup.AsNoTracking().AsQueryable();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(IEnumerable<StationedCompanyGroup> item)
        {
            db.StationedCompanyGroup.AddRange(item);
            int result = db.SaveChanges();

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(StationedCompanyGroup item)
        {
            StationedCompanyGroup updateItem = db.StationedCompanyGroup.Find(item.Id);
            db.Entry(updateItem).CurrentValues.SetValues(item);
            int result = db.SaveChanges();

            return result;
        }

        #region Dispose

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        #endregion Dispose
    }
}