﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models
{
    [Table("LivingArea")]
    public partial class LivingArea
    {
        [Key]
        [StringLength(50)]
        public string Id { get; set; }

        [Required]
        [StringLength(10)]
        [Description("縣市代碼")]
        public string CityCode { get; set; }

        [StringLength(50)]
        [Description("縣市名稱")]
        public string CityName { get; set; }

        [Required]
        [StringLength(10)]
        [Description("鄉、鎮市區代碼")]
        public string DistrictCode { get; set; }

        [StringLength(50)]
        [Description("鄉、鎮市區名稱")]
        public string DistrictName { get; set; }

        [Required]
        [StringLength(10)]
        [Description("郵遞區號")]
        public string PostalCode { get; set; }
    }
}