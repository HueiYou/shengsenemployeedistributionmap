﻿using ShengsenEmployeeDistributionMap.Models.BLO;
using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using ShengsenEmployeeDistributionMap.Models.ViewModel.UniForm;
using System;
using System.Net;
using System.Web.Http;

namespace ShengsenEmployeeDistributionMap.Api
{
    /// <summary>
    /// 群組Api
    /// </summary>
    [Authorize]
    public class GroupApiController : ApiController
    {
        private readonly GroupBLO _groupBLO;

        /// <summary>
        /// Constructor
        /// </summary>
        public GroupApiController()
        {
            _groupBLO = new GroupBLO();
        }

        /// <summary>
        /// 取得群組清單
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public OutUniResult GetGroupList([FromBody]InGroupSearchViewModel param)
        {
            try
            {
                return new OutUniResult(HttpStatusCode.OK, _groupBLO.GetGroupList(param), null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.BadRequest, null, ex.ToString());
            }
        }

        /// <summary>
        /// 建立群組
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public OutUniResult CreateGroup([FromBody]InGroupCreateViewModel param)
        {
            try
            {
                _groupBLO.CreateGroup(param);
                return new OutUniResult(HttpStatusCode.OK, null, null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.BadRequest, null, ex.ToString());
            }
        }

        /// <summary>
        /// 更新群組
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public OutUniResult UpdateGroup([FromBody]InGroupCreateViewModel param)
        {
            try
            {
                _groupBLO.UpdateGroup(param);
                return new OutUniResult(HttpStatusCode.OK, null, null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.BadRequest, null, ex.ToString());
            }
        }

        /// <summary>
        /// 刪除群組
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public OutUniResult DeleteGroup([FromBody]InGroupCreateViewModel param)
        {
            try
            {
                _groupBLO.DeleteGroup(param);
                return new OutUniResult(HttpStatusCode.OK, null, null);
            }
            catch (Exception ex)
            {
                return new OutUniResult(HttpStatusCode.BadRequest, null, ex.ToString());
            }
        }
    }
}