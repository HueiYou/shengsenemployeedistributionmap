﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.Report
{
    public class Report_LivingArea
    {
        /// <summary>
        /// 縣市代碼
        /// </summary>
        public string CityCode { get; set; }

        /// <summary>
        /// 縣市名稱
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 鄉、鎮市區代碼
        /// </summary>
        public string DistrictCode { get; set; }

        /// <summary>
        /// 鄉、鎮市區名稱
        /// </summary>
        public string DistrictName { get; set; }

    }
}