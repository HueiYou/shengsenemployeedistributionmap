﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    /// <summary>
    /// 居住地區驗證用ViewModel
    /// </summary>
    public class InLivingAreaValidationViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(10, ErrorMessage = "{0} 不可超過10字元")]
        [Display(Name = "縣市代碼")]
        public string CityCode { get; set; }

        [StringLength(50, ErrorMessage = "{0} 不可超過50字元")]
        [Display(Name = "縣市名稱")]
        public string CityName { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(10, ErrorMessage = "{0} 不可超過10字元")]
        [Display(Name = "鄉、鎮市區代碼")]
        public string DistrictCode { get; set; }

        [StringLength(50, ErrorMessage = "{0} 不可超過50字元")]
        [Display(Name = "鄉、鎮市區名稱")]
        public string DistrictName { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(10, ErrorMessage = "{0} 不可超過10字元")]
        [Display(Name = "郵遞區號")]
        public string PostalCode { get; set; }
    }
}