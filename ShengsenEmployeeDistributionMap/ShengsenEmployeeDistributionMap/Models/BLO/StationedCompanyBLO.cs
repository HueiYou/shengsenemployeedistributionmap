﻿using ShengsenEmployeeDistributionMap.Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShengsenEmployeeDistributionMap.Models.BLO
{
    /// <summary>
    /// 駐點公司BLO
    /// </summary>
    public class StationedCompanyBLO
    {
        private readonly StationedCompanyDAO _StationedCompanyDAO;
        public StationedCompanyBLO()
        {
            _StationedCompanyDAO = new StationedCompanyDAO();
        }

        /// <summary>
        /// 取得所有資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<StationedCompany> GetAll()
        {
            return _StationedCompanyDAO.GetAll();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Create(StationedCompany item)
        {
            int result = _StationedCompanyDAO.Create(item);

            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Update(StationedCompany item)
        {
            int result = _StationedCompanyDAO.Update(item);

            return result;
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Delete(StationedCompany item)
        {
            int result = _StationedCompanyDAO.Delete(item);

            return result;
        }

        /// <summary>
        /// 取得駐點公司下拉選單
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetStationedCompanySelectListItems()
        {
            IEnumerable<SelectListItem> listItems = _StationedCompanyDAO.GetAll()
                .Select(x => new SelectListItem()
                {
                    Value = x.Id,
                    Text = x.CompanySimpleName,
                })
                .OrderBy(x => x.Text)
                .ToList();

            if (listItems == null)
                return new List<SelectListItem>();
            else
                return listItems;
        }

        /// <summary>
        /// 取得駐點公司過濾資料
        /// </summary>
        /// <returns></returns>
        public IQueryable<StationedCompany> GetFilterData(string CompanySimpleName, string CompanyName, string CompanyAddress)
        {
            IQueryable<StationedCompany> query = _StationedCompanyDAO.GetAll();

            if (!string.IsNullOrEmpty(CompanySimpleName))
                query = query.Where(x => x.CompanySimpleName.Contains(CompanySimpleName));
            if (!string.IsNullOrEmpty(CompanyName))
                query = query.Where(x => x.CompanyName.Contains(CompanyName));
            if (!string.IsNullOrEmpty(CompanyAddress))
                query = query.Where(x => x.CompanyAddress.Contains(CompanyAddress));

            return query.OrderBy(x => x.CompanySimpleName);
        }
    }
}