﻿using System;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.Out
{
    public class OutSatisfactionSurveyPageViewModel
    {
        /// <summary>
        /// 編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 員工編號
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// 員工姓名
        /// </summary>
        public string EmployeeName { get; set; }

        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string StationedCompanyId { get; set; }

        /// <summary>
        /// 駐點公司名稱
        /// </summary>
        public string StationedName { get; set; }

        /// <summary>
        /// 評估期間
        /// </summary>
        public DateTime? EvaluateDate { get; set; }

        /// <summary>
        /// 評核人員
        /// </summary>
        public string Appraiser { get; set; }

        /// <summary>
        /// 評核日期
        /// </summary>
        public DateTime? AppraiseDate { get; set; }
    }
}