namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateTable : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Employees");
            DropPrimaryKey("dbo.LivingArea");
            DropPrimaryKey("dbo.StatuinedCompany");
            AlterColumn("dbo.Employees", "Id", c => c.String(nullable: false, maxLength: 16));
            AlterColumn("dbo.LivingArea", "Id", c => c.String(nullable: false, maxLength: 16));
            AlterColumn("dbo.StatuinedCompany", "Id", c => c.String(nullable: false, maxLength: 16));
            AddPrimaryKey("dbo.Employees", "Id");
            AddPrimaryKey("dbo.LivingArea", "Id");
            AddPrimaryKey("dbo.StatuinedCompany", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.StatuinedCompany");
            DropPrimaryKey("dbo.LivingArea");
            DropPrimaryKey("dbo.Employees");
            AlterColumn("dbo.StatuinedCompany", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.LivingArea", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Employees", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.StatuinedCompany", "Id");
            AddPrimaryKey("dbo.LivingArea", "Id");
            AddPrimaryKey("dbo.Employees", "Id");
        }
    }
}
