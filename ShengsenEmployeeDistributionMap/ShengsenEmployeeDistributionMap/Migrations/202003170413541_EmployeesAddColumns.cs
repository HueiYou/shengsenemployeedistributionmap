namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeesAddColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "Category", c => c.Int(nullable: false));
            AddColumn("dbo.Employees", "Personality", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.Employees", "Education", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.Employees", "WorkExperience", c => c.String(nullable: false));
            AddColumn("dbo.Employees", "StartWorkDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Employees", "Residence", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "Residence");
            DropColumn("dbo.Employees", "StartWorkDate");
            DropColumn("dbo.Employees", "WorkExperience");
            DropColumn("dbo.Employees", "Education");
            DropColumn("dbo.Employees", "Personality");
            DropColumn("dbo.Employees", "Category");
        }
    }
}
