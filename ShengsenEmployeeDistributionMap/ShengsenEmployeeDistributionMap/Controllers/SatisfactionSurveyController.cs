﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShengsenEmployeeDistributionMap.Controllers
{
    public class SatisfactionSurveyController : Controller
    {
        public SatisfactionSurveyController()
        {

        }

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult SurveyPage()
        {
            return View();
        }
    }
}