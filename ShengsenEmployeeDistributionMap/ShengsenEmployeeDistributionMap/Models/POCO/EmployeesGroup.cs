﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShengsenEmployeeDistributionMap.Models
{
    [Table("EmployeesGroup")]
    public partial class EmployeesGroup
    {
        /// <summary>
        /// 員工群組編號
        /// </summary>
        [Key]
        [StringLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 群組編號
        /// </summary>
        [Required]
        [StringLength(50)]
        [ForeignKey("Group")]
        public string GroupId { get; set; }

        public virtual Group Group { get; set; }

        /// <summary>
        /// 員工編號
        /// </summary>
        [Required]
        [StringLength(50)]
        public string EmployeeId { get; set; }
    }
}