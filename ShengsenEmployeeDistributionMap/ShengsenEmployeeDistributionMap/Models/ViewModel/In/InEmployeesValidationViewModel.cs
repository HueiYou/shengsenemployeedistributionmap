﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    /// <summary>
    /// 員工資訊驗證用ViewModel
    /// </summary>
    public class InEmployeesValidationViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(50, ErrorMessage = "{0} 不可超過50字元")]
        [Display(Name = "員工姓名")]
        public string EmployeeName { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(10, ErrorMessage = "{0} 不可超過10字元")]
        [Display(Name = "縣市")]
        public string CityCode { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(10, ErrorMessage = "{0} 不可超過10字元")]
        [Display(Name = "鄉、鎮市區")]
        public string DistrictCode { get; set; }

        [StringLength(50, ErrorMessage = "{0} 不可超過50字元")]
        [Display(Name = "駐點公司")]
        public string CompanyId { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(10, ErrorMessage = "{0} 不可超過10字元")]
        [Display(Name = "技能")]
        public string Department { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [Range(0, 999, ErrorMessage = "{0}範圍限制在{1}~{2}之間")]
        [Display(Name = "類別(來源)")]
        public int Category { get; set; }

        [StringLength(20, ErrorMessage = "{0} 不可超過20字元")]
        [Display(Name = "四型人格")]
        public string Personality { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [StringLength(100, ErrorMessage = "{0} 不可超過100字元")]
        [Display(Name = "學歷/學位")]
        public string Education { get; set; }
        
        [MaxLength]
        [Display(Name = "經歷")]
        public string WorkExperience { get; set; }

        [Required(ErrorMessage = "該欄位必填")]
        [Display(Name = "工程師總年資")]
        [DataType(DataType.Date)]
        public DateTime StartWorkDate { get; set; }
        public string StartWorkDateString { get; set; }

        [MaxLength]
        [Display(Name = "住所地址")]
        public string Residence { get; set; }

        [Required]
        [Display(Name = "性別")]
        public int Gender { get; set; }

        [MaxLength]
        [Display(Name = "技能描述")]
        public string SkillDescription { get; set; }

        [MaxLength]
        [Display(Name = "備註")]
        public string Remark { get; set; }

        [Display(Name = "聖森到職日期")]
        [DataType(DataType.Date)]
        public DateTime? OnBoardDate { get; set; }
        public string OnBoardDateString { get; set; }

        [Display(Name = "駐點日期")]
        [DataType(DataType.Date)]
        public DateTime? LocationedDate { get; set; }
        public string LocationedDateString { get; set; }

        [Display(Name = "出生年月日")]
        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }
        public string BirthdayString { get; set; }

        [Display(Name = "建立日期")]
        [DataType(DataType.Date)]
        public DateTime? CreateDate { get; set; }

        public IEnumerable<SelectListItem> CitySelectListItems { get; set; }
        public IEnumerable<SelectListItem> DistrictSelectListItems { get; set; }
        public IEnumerable<SelectListItem> StationedCompanySelectListItems { get; set; }
    }
}