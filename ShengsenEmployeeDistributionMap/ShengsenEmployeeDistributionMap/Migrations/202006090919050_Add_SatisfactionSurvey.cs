namespace ShengsenEmployeeDistributionMap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_SatisfactionSurvey : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SatisfactionSurvey",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 50),
                        EmployeeId = c.String(nullable: false, maxLength: 50),
                        StationedCompanyId = c.String(nullable: false, maxLength: 50),
                        EvaluateDate = c.DateTime(),
                        EnthusiasmScore = c.Int(nullable: false),
                        EnthusiasmDesc = c.String(maxLength: 100),
                        CommunicationScore = c.Int(nullable: false),
                        CommunicationDesc = c.String(maxLength: 100),
                        ResolveScore = c.Int(nullable: false),
                        ResolveDesc = c.String(maxLength: 100),
                        ProfessionScore = c.Int(nullable: false),
                        ProfessionDesc = c.String(maxLength: 100),
                        TimeControlScore = c.Int(nullable: false),
                        TimeControlDesc = c.String(maxLength: 100),
                        Comment = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SatisfactionSurvey");
        }
    }
}
