﻿namespace ShengsenEmployeeDistributionMap.Models.ViewModel.In
{
    public class InEmployeesGroupListViewModel
    {
        /// <summary>
        /// 員工編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 員工姓名
        /// </summary>
        public string EmployeeName { get; set; }

        /// <summary>
        /// 群組編號
        /// </summary>
        public string GroupId { get; set; }
    }
}