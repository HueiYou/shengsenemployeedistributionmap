﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.ViewModel.Report
{
    public class Report_StationedCompany
    {
        /// <summary>
        /// 駐點公司編號
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 駐點公司簡稱
        /// </summary>
        public string CompanySimpleName { get; set; }
    }
}