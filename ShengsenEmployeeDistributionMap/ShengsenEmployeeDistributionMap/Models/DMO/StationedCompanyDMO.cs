﻿using ShengsenEmployeeDistributionMap.Models.ViewModel.In;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShengsenEmployeeDistributionMap.Models.DMO
{
    /// <summary>
    /// 駐點公司DMO
    /// </summary>
    public static class StationedCompanyDMO
    {
        /// <summary>
        /// 將POCO轉成ViewModel
        /// </summary>
        /// <param name="stationedCompany"></param>
        /// <returns></returns>
        public static InStationedCompanyValidationViewModel ToInStationedCompanyValidationViewModel(this StationedCompany stationedCompany)
        {
            InStationedCompanyValidationViewModel result = new InStationedCompanyValidationViewModel()
            {
                Id = stationedCompany.Id,
                CompanySimpleName = stationedCompany.CompanySimpleName,
                CompanyName = stationedCompany.CompanyName,
                CompanyAddress = stationedCompany.CompanyAddress,
            };

            return result;
        }

        /// <summary>
        /// 將ViewModel轉成POCO
        /// </summary>
        /// <param name="stationedCompany"></param>
        /// <returns></returns>
        public static StationedCompany ToStationedCompany(this InStationedCompanyValidationViewModel stationedCompany)
        {
            StationedCompany result = new StationedCompany()
            {
                Id = stationedCompany.Id,
                CompanySimpleName = stationedCompany.CompanySimpleName,
                CompanyName = stationedCompany.CompanyName,
                CompanyAddress = stationedCompany.CompanyAddress,
            };

            return result;
        }
    }
}